<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Incident Notification and Distribution Portal</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('bootstrap-3.3.7-dist/css/bootstrap.min.css') }}" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{ URL::asset('bootstrap-3.3.7-dist/assets/css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('bootstrap-3.3.7-dist/css/dashboard.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('bootstrap-3.3.7-dist/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Tag Input-->
    <link rel="stylesheet" href="{{ URL::asset('tag_input/css/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('tag_input/assets/app.css') }}">


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->

    <!--[if lt IE 9]>
    <script src=" {{ URL::asset('bootstrap-3.3.7-dist/assets/js/ie8-responsive-file-warning.js')   }}" type="text/javascript"></script>
    <![endif]-->

    <script src=" {{ URL::asset('bootstrap-3.3.7-dist/assets/js/ie-emulation-modes-warning.js')   }}" ></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<style>
    /*
   * Style tweaks
   * --------------------------------------------------
   */
    body {
        padding-top: 50px;
        background-color: #f5f5f5;
    }
    footer {
        padding-left: 15px;
        padding-right: 15px;
        background-color: #fff;
    }

    /*
     * Off Canvas
     * --------------------------------------------------
     */
    @media screen and (max-width: 768px) {
        .row-offcanvas {
            position: relative;
            -webkit-transition: all 0.25s ease-out;
            -moz-transition: all 0.25s ease-out;
            transition: all 0.25s ease-out;
        }

        .row-offcanvas-left
        .sidebar-offcanvas {
            left: -33%;
        }

        .row-offcanvas-left.active {
            left: 33%;
        }

        .sidebar-offcanvas {
            position: absolute;
            top: 0;
            width: 33%;
            margin-left: 10px;
        }
    }


    /* Sidebar navigation */
    .nav-sidebar {
        background-color: #f5f5f5;
        margin-right: -15px;
        margin-bottom: 20px;
        margin-left: -15px;
    }
    .nav-sidebar > li > a {
        padding-right: 20px;
        padding-left: 20px;
    }
    .nav-sidebar > .active > a {
        color: #fff;
        background-color: #428bca;
    }

    /*
     * Main content
     */

    .main {
        padding: 20px;
        background-color: #fff;
    }
    @media (min-width: 768px) {
        .main {
            padding-right: 40px;
            padding-left: 40px;
        }
    }
    .main .page-header {
        margin-top: 0;
    }

    .span12{
        border:solid 2px black;
        background-color:grey;
    }
</style>


<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Portal</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Settings</a></li>
                <li><a href="#">Profile</a></li>
                <li><a href="{{ route('auth.logout') }}">Logout</a></li>
            </ul>
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-left">

        <div class="col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">

        <ul class="nav nav-sidebar">
                <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li>
                <li id="loghref"><a href="/log">Activity</a></li>
                <li><a href="#">Export</a></li>
            </ul>

            <ul class="nav nav-sidebar">
                <li id="userhref"><a href="#" id="btn-1" data-toggle="collapse" data-target="#submenu1" aria-expanded="false">User</a>
                    <ul class="nav collapse" id="submenu1" role="menu" aria-labelledby="btn-1">
                        <li><a href="{{ route('user.create') }}">Create</a></li>
                        <li><a href="{{ route('user.index') }}">View</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav nav-sidebar">
                <li id="userhref"><a href="#" id="btn-1" data-toggle="collapse" data-target="#submenu2" aria-expanded="false">Incident</a>
                    <ul class="nav collapse" id="submenu2" role="menu" aria-labelledby="btn-1">
                        <li><a href="{{ route('incident.create') }}">Create</a></li>
                        <li><a href="{{ route('incident.index') }}">View</a></li>
                        <li><a href="{{ route('incident.assigned') }}">Assigned</a></li>

                    </ul>
                </li>
            </ul>

            <ul class="nav nav-sidebar">
                <li id="reporthref"><a href="#" id="btn-1" data-toggle="collapse" data-target="#submenu2" aria-expanded="false">Report</a>
                    <ul class="nav collapse" id="submenu2" role="menu" aria-labelledby="btn-1">
                        <li><a href="{{ route('incident.create') }}">Create</a></li>
                        <li><a href="{{ route('incident.index') }}">View</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav nav-sidebar">
                <li><a href="">Nav item</a></li>
                <li><a href="">Nav item again</a></li>
                <li><a href="">One more nav</a></li>
                <li><a href="">Another nav item</a></li>
                <li><a href="">More navigation</a></li>
            </ul>
            <ul class="nav nav-sidebar">
                <li><a href="">Nav item again</a></li>
                <li><a href="">One more nav</a></li>
                <li><a href="">Another nav item</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10 main">

            <!--toggle sidebar button-->
            <p class="visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><i class="glyphicon glyphicon-chevron-left"></i></button>
            </p>

            @yield('content')
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src=" {{ URL::asset('jquery/jquery-1.9.1.js')   }}" type="text/javascript"></script>
<script type="text/javascript" src=" {{ URL::asset('bootstrap-3.3.7-dist/js/Moment.js')   }}"></script>
<script src=" {{ URL::asset('bootstrap-3.3.7-dist/js/transition.js')   }}" type="text/javascript"></script>
<script src=" {{ URL::asset('bootstrap-3.3.7-dist/js/collapse.js')   }}" type="text/javascript"></script>
<script src=" {{ URL::asset('bootstrap-3.3.7-dist/js/datetimepicker.min.js')   }}" type="text/javascript"></script>
<script src=" {{ URL::asset('bootstrap-3.3.7-dist/js/bootstrap.min.js')  }}" type="text/javascript"></script>

<!-- tag input -->
<script src="{{ URL::asset('tag_input/js/bootstrap-tagsinput.min.js') }}"></script>
<!-- tag input -->

<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src=" {{ URL::asset('bootstrap-3.3.7-dist/assets/js/vendor/holder.min.js')   }}" type="text/javascript"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src=" {{ URL::asset('bootstrap-3.3.7-dist/assets/js/ie10-viewport-bug-workaround.js')   }}" type="text/javascript"></script>

@yield('contentJS')

<script type="text/javascript">
    $(document).ready(function() {


        $('[data-toggle=offcanvas]').click(function() {
            $('.row-offcanvas').toggleClass('active');
        });
    });

</script>

</body>
</html>
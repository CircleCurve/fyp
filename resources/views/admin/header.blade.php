<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>D</b>T</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>D</b>Team</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">{{ count($stillValidIncidents[0]->calendar) }}</span>
                    </a>
                    <ul class="dropdown-menu">


                        <li class="header">You have {{ count($stillValidIncidents[0]->calendar) }} messages</li>


                        @foreach($stillValidIncidents as $user)
                            @foreach($user->calendar as $calendar)

                        <!-- <li class="header">You have 4 messages</li>-->
                                <li>
                                    <!-- inner menu: contains the messages -->
                                    <ul class="menu">
                                        <li><!-- start message -->
                                            <a href="/incident/{{ $calendar->incident->id }}">
                                                <div class="pull-left">
                                                    <!-- User Image -->
                                                    <img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ strtoupper(substr($calendar->incident->user->name, 0, 1)) }}" class="img-circle" alt="User Image">
                                                </div>
                                                <!-- Message title and timestamp -->
                                                <h4>
                                                    {{  $calendar->incident->title }}
                                                    <small><i class="fa fa-clock-o"></i>
                                                        @if ( isset ($calendar->incident->progress[0]))

                                                            {{  explode(" ",$calendar->incident->progress[0]->pivot->updated_at)[0] }}
                                                        @endif</small>
                                                </h4>
                                                <!-- The message -->
                                                <p>{{ $calendar->incident->progress[0]->description }}</p>
                                            </a>
                                        </li>
                                        <!-- end message -->
                                    </ul>
                                    <!-- /.menu -->
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                             @endforeach
                        @endforeach
                    </ul>
                </li>
                <!-- /.messages-menu -->

                <!-- Notifications Menu -->
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ strtoupper(substr(Auth::user()->name, 0, 1)) }}" class="user-image" alt="User Image">

                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ strtoupper(substr(Auth::user()->name, 0, 1)) }}" class="img-circle" alt="User Image">

                            <p>
                                {{ Auth::user()->name ."-" . Auth::user()->jobTitle  }}
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!--
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                            <!-- /.row -
                        </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">

                            <div class="pull-right">
                                <a href="{{ route('auth.logout') }}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
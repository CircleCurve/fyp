<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">

                <?php /*<img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ Auth::user()->name[0] }}" class="img-circle" alt="User Image">*/ ?>
                <img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ strtoupper(substr(Auth::user()->name, 0, 1)) }}"
                     class="img-circle" alt="User Image">

            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Main Information</li>
            <!-- Optionally, you can add icons to the links -->

            @role('admin')
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('user.index')  }}"><i class="fa fa-user"></i>User</a></li>
                    <li><a href="{{ route('role.index')  }}"><i class="fa fa-user"></i>Role</a></li>
                    <li><a href="{{ route('perm.index')  }}"><i class="fa fa-user"></i>Pemission</a></li>
                    <li><a href="{{ route('log.index')  }}"><i class="fa fa-users"></i>Activity Log</a></li>
                </ul>
            </li>
            @endrole('admin')
            {{--
            @role('admin')
            <li class="treeview">
                <a href="#"><i class="fa fa-toggle-on"></i> <span>Distribute</span>
                                    <span class="pull-right-container">
                                      <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-arrows-v"></i>Method Selection</a></li>
                </ul>
            </li>
            @endrole
            --}}

            @role(['admin', 'front-end','developer'])
            <li class="treeview">
                <a href="#"><i class="fa fa-ticket"></i> <span>Incident</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('incident.create')  }}"><i class="fa fa-circle-o"></i>Create</a></li>
                    <li><a href="{{ route('incident.index')  }}"><i class="fa fa-circle-o"></i>View</a></li>
                    <li><a href="{{ route('incident.assigned')  }}"><i class="fa fa-circle-o"></i>Assigned</a></li>
                </ul>
            </li>
            @endrole

            @role(['admin','analysis'])
            <li class="">
                <a href="{{ route('report.index')  }}">
                    <i class="fa fa-line-chart"></i>
                    <span>Report</span>
                </a>
            </li>
            @endrole

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->

</aside>

@section('menuJS')
    <script>
        // Set active state on menu element
        var current_url = "{{ url(Route::current()->getUri()) }}";
        $("ul.sidebar-menu li a").each(function () {
            if ($(this).attr('href').startsWith(current_url) || current_url.startsWith($(this).attr('href'))) {
                $(this).parents('li').addClass('active');
            }
        });
    </script>
@endsection
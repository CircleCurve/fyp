<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">

            <!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Receive Incident Progress</h3>
            <ul class="control-sidebar-menu">

                @foreach($stillValidIncidents as $user)
                    @foreach($user->calendar as $calendar)
                        <li>
                            <a href="/incident/{{ $calendar->incident->id }}">
                                <h4 class="control-sidebar-subheading">
                                       {{  $calendar->incident->title }}
                                <span class="pull-right-container">
                                  <span class="label label-danger pull-right">
                                          @if ( isset ($calendar->incident->progress[0]))
                                          {{
                                          Config::get('constant.calendar.setting.'.
                                          ($calendar->incident->progress[0]->id ).
                                          '.percentage')
                                          }}
                                          @endif

                                  </span>
                                </span>
                                </h4>

                                <div class="progress progress-xxs">

                                    @if ( isset ($calendar->incident->progress[0]))
                                        <div class="progress-bar progress-bar-danger" style="width: {{
                                        Config::get('constant.calendar.setting.'.
                                        ($calendar->incident->progress[0]->id ).
                                        '.percentage')
                                        }}"></div>


                                    @endif

                                </div>
                            </a>
                        </li>
                    @endforeach

                @endforeach

            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <h3 class="control-sidebar-heading">Assigned Incident Progress</h3>
            <ul class="control-sidebar-menu">

                @foreach($stillValidIncidentsAssign as $users)
                    @foreach($users->incident as $incident)
                        <li>
                            <a href="/incident/{{ $incident->calendar->id }}">
                                <h4 class="control-sidebar-subheading">
                                    {{  $incident->calendar->title }}
                                    <span class="pull-right-container">
                                  <span class="label label-danger pull-right">
                                          @if ( isset ($incident->calendar->progress[0]))
                                          {{
                                          Config::get('constant.calendar.setting.'.
                                          ($incident->calendar->progress[0]->id ).
                                          '.percentage')
                                          }}
                                      @endif

                                  </span>
                                </span>
                                </h4>

                                <div class="progress progress-xxs">

                                    @if ( isset ($incident->calendar->progress[0]))
                                        <div class="progress-bar progress-bar-danger" style="width: {{
                                        Config::get('constant.calendar.setting.'.
                                        ($incident->calendar->progress[0]->id ).
                                        '.percentage')
                                        }}"></div>


                                    @endif

                                </div>
                            </a>
                        </li>
                    @endforeach

                @endforeach

            </ul>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<div class="control-sidebar-bg"></div>
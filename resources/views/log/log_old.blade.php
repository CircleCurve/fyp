@extends('layouts.dashboard')
@section('content')


<div class="panel panel-success">
    <div class="panel-heading">Search</div>
    <div class="panel-body">
        <form method="POST" action="/log">

            {!! csrf_field() !!}
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>User Name</label>

                            <input type="text" class="form-control" name="userName" id="userName" value="{{ old('userName') }}" placeholder="Please input the part of user name">
                    </div>
                    <div class="col-md-6">
                        <label>Description</label>

                            <input type="text" class="form-control" name="description" id="description" value="{{ old('description') }}" placeholder="Please input the part of description">
                    </div>
                </div>
           </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Start Date (Only for Today)</label>
                        <div class='input-group date' id='datetimepicker1'>

                            <input type="text" class="form-control" name="startDate" id="startDate" value="{{ old('startDate') }}" readonly>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label>End Date</label>
                        <div class='input-group date' id='datetimepicker2'>

                            <input type="text" class="form-control" name="endDate" id="endDate" value="{{ old('endDate') }}" readonly>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-success pull-right" type="submit">Submit</button>
            </div>

        </form>
    </div>

</div>



<div class="panel panel-primary">
    <!-- Default panel contents -->
    <div class="panel-heading">Activity Log</div>

    <!-- Table -->
    <div class="table-responsive">

        <table class="table table-hover table-striped">
            <thread>
                <tr>
                    <td>Log id</td>
                    <td>User name</td>
                    <td>description</td>
                    <td>Log Time</td>
                    <td>IP</td>
                </tr>
            </thread>

            <tbody>
            @foreach ($logs as $log)
                <tr>
                    <td>{{$log->id}}</td>
                    <td>{{$log->User->name}}</td>
                    <td>{{$log->description}}</td>
                    <td>{{$log->created_at}}</td>
                    <td>{{$log->ip}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
@endsection

@section('contentJS')
    <script type="text/javascript">
        $(document).ready(function() {

            $('#datetimepicker1').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                minDate:new Date()
            });
            $('#datetimepicker2').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                minDate:new Date(),
            });

            $('.nav li').removeClass('active');

            var $parent = $('#loghref');
            if (!$parent.hasClass('active')) {
                $parent.addClass('active');
            }
        })
    </script>
@endsection
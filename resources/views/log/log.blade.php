@extends('admin.template')

@section('contentHeader')

    <h1>
        User Management
        <small>view log information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('log.index') }}"><i class="fa fa-dashboard"></i> Log</a></li>
        <li class="active">View log</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')

    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header with-border">

                </div>
                <div class="box-body">
                    <div class="form-group">

                        <div class="row">
                            <!-- Date range -->
                            <div class="col-md-12">
                                <label>Last Login Between:</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="reservation" readonly>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <!-- Date range -->
                            <div class="col-md-12">
                                <button id="clearFilter" class="btn btn-success pull-right">clearFilter</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="filterDataTable" class="table  table-hover table-striped">

                            <thead>
                            <tr>
                                <th>Log id</th>
                                <th>Uid</th>
                                <th>User name</th>
                                <th>Description</th>
                                <th>Log Time</th>
                                <th>IP</th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th class="need">Log id</th>
                                <th class="need">Uid</th>
                                <th class="need">User name</th>
                                <th class="need">Description</th>
                                <th></th>
                                <th class="need">IP</th>

                            </tr>
                            </tfoot>

                            <tbody>
                            @foreach ($logs as $log)
                                <tr>
                                    <td>{{$log->id}}</td>
                                    @if (isset($log->User->id))
                                        <td>{{$log->User->id}}</td>
                                        <td>{{$log->User->name}}</td>
                                    @endif
                                    <td>{{$log->description}}</td>
                                    <td>{{$log->created_at}}</td>
                                    <td>{{$log->ip}}</td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@endsection

@section('contentJS')
    <script>

        $(function () {
            filterDataTable("filterDataTable", 4);
        });

    </script>

@endsection
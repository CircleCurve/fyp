@extends('admin.template')

@section('contentHeader')

    <h1>
        Role Management
        <small>view role information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('role.index') }}"><i class="fa fa-dashboard"></i> Role</a></li>
        <li class="active">View role</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <a href="{{ route('role.create') }}">
                            <button type="submit" class="btn btn-primary">Add Role</button>
                        </a>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="roleTable" class="table  table-hover table-striped">

                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Display_name</th>
                                    <th>Description</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                    <th>Remove</th>

                                </tr>
                            </thead>
                            <tbody>

                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $role->id }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->display_name }}</td>
                                        <td>{{ $role->description }}</td>
                                        <td>{{ $role->created_at }}</td>
                                        <td>{{ $role->updated_at }}</td>
                                        <td><button type="button" class="btn btn-warning" onclick="javascript:location.href='/role/{{ $role->id }}'">Detail</button></td>
                                        <td>
                                            <form method="POST" action="{{ route('role.destroy', $role->id) }}">
                                                {!! csrf_field() !!}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger">Remove</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>


@endsection

@section('contentJS')
<script>

    $(function () {


        $('#roleTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });


</script>

@endsection

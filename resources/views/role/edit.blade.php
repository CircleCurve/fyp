@extends('admin.template')

@section('contentHeader')

    <h1>
        Role Management
        <small>edit role information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('role.index') }}"><i class="fa fa-dashboard"></i> Role</a></li>
        <li class="active">Edit role</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')



    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit a role</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="POST" action="{{ route('role.update',$role->id ) }}">
                    {{ method_field('PUT') }}
                    {!! csrf_field() !!}

                    <div class="box-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label>Name</label>

                            <input type="text" class="form-control" name="name" value="{{ old('name')?old('name'):$role->name }}">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                            <label>Display name</label>
                            <input type="text" class="form-control" name="display_name" value="{{ old('display_name')?old('display_name'):$role->display_name }}">

                            @if ($errors->has('display_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('display_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label>Description</label>
                            <input type="text" class="form-control" name="description" value="{{ old('description')?old('description'):$role->description }}">

                            @if ($errors->has('description'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description')?:$role->description  }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="row form-group{{ $errors->has('perms') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <label>Permission</label>

                                @if ($errors->has('perms'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('perms') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="hidden_fields_primary" data-name = "role"></div>

                            @foreach($perms as $perm)
                                <div class="col-sm-4">
                                    <div class="checkbox">
                                        <label>

                                            <input type="checkbox" value="{{ $perm->id }}" name="perms[]"  data-id="{{$perm->id}}" class="secondary_list"> {{ $perm->name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>


                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('contentJS')
    <script>
        $(document).ready(function(){
            var rolePermission = JSON.parse('{!! $rolePermission !!}')


            $.each(rolePermission, function(i,v){
                console.log("rolePermission : " + v);
                $('.secondary_list[value="'+v+'"]').prop('checked', true);
            });

        })


    </script>

@endsection
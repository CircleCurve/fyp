<html>
<head></head>
<body style="text-align: center">
<h1>{{$title}}</h1>

<br/>

<p>{{$user->name. " send to you about ". $title}}</p>

<p>App Information</p>
<table width="100%" border="3px" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">App id</td>
        <td align="center">Description</td>
    </tr>
    <tr>
        <td align="center">{{$appId}}</td>
        <td align="center">{{$description}}</td>
    </tr>

</table>

<p>Group of Issue Information</p>

<table width="100%" border="3px" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">Id</td>
        <td align="center">Symptom</td>

    </tr>
    {!! $symptom_body !!}
</table>

<p>For more details : Please refer to this link : {{ $link }}</p>

</body>
</html>
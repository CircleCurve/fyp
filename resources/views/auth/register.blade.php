@extends('admin.template')

@section('contentHeader')

    <h1>
        User Management
        <small>add a New User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('user.index') }}"><i class="fa fa-dashboard"></i> User</a></li>
        <li class="active">Add user</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add a New User</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="POST" action="{{ route('user.store') }}">
                    {!! csrf_field() !!}

                    <div class="box-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label>Name</label>

                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>E-Mail Address</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" name="password_confirmation">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="row form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <label>Roles</label>

                                @if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="hidden_fields_primary" data-name = "role"></div>

                            @foreach($roles as $role)
                                <div class="col-sm-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="{{ $role->id }}" class="primary_list" data-id="{{ $role->id }}">
                                            {{ $role->name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!--
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Permission</label>
                            </div>

                            <div data-name="permissions" class="hidden_fields_secondary">
                            </div>

                            @foreach($perms as $perm)
                                <div class="col-sm-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="{{ $perm->id }}" name="permissions_show[]"  data-id="{{$perm->id}}" class="secondary_list"> {{ $perm->name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach

                        </div>-->


                        <div class="row">
                            <div class="col-xs-12">
                                <label>Verification</label>
                                <div class="g-recaptcha" data-sitekey="6LfU9gMTAAAAAOZZY4UeHQuaLcFUdn9BL0-5wINz"></div>

                            </div>


                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('contentJS')
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>
        $(document).ready(function() {
            $('.primary_list').change(function () {
                //var  user_role_permission = {"1":[1,2,3],"2":[2],"3":[2],"4":[3]};
                var user_role_permission = JSON.parse('{!! $user_role_permission !!}');
                var idCurrent = $(this).data('id');


                if ($(this).is(':checked')) {

                    //add hidden field with this value
                    var nameInput = $('.hidden_fields_primary').data('name');
                    var inputToAdd = $('<input type="hidden" class="primary_hidden" name="' + nameInput + '[]" value="' + idCurrent + '">');

                    $('.hidden_fields_primary').append(inputToAdd);

                    $.each(user_role_permission[idCurrent], function (key, value) {
                        //check and disable secondies checkbox
                        $('.secondary_list[value="'+value+'"]').prop("checked", true);
                        $('.secondary_list[value="'+value+'"]').prop("disabled", true);
                        //remove hidden fields with secondary dependency if was setted

                        var hidden = $('.secondary_list_hidden[value="'+value+'"]');
                        if (hidden)
                            hidden.remove();
                    });

                } else {
                    //remove hidden field with this value
                    $('.primary_hidden[value="' + idCurrent + '"]').remove();

                    // uncheck and active secondary checkboxs if are not in other selected primary.

                    var dependencyJson = user_role_permission;

                    var selected = [];
                    $('.primary_hidden').each(function (index, input) {
                        selected.push($(this).val());
                    });

                    $.each(dependencyJson[idCurrent], function (index, secondaryItem) {
                        var ok = 1;

                        $.each(selected, function (index2, selectedItem) {
                            if (dependencyJson[selectedItem].indexOf(secondaryItem) != -1) {
                                ok = 0;
                            }
                        });

                        if (ok) {
                            $('.secondary_list[value="' + secondaryItem + '"]').prop('checked', false);
                            $('.secondary_list[value="' + secondaryItem + '"]').prop('disabled', false);
                        }
                    });

                }
            });


            $('.secondary_list').click(function () {

                var idCurrent = $(this).data('id');
                if ($(this).is(':checked')) {
                    //add hidden field with this value
                    var nameInput = $('.hidden_fields_secondary').data('name');
                    var inputToAdd = $('<input type="hidden" class="secondary_hidden" name="' + nameInput + '[]" value="' + idCurrent + '">');

                    $('.hidden_fields_secondary').append(inputToAdd);

                } else {
                    //remove hidden field with this value
                    $('input.secondary_hidden[value="' + idCurrent + '"]').remove();
                }
            });
        });
    </script>
@endsection
@extends('layouts.dashboard')

@section('content')
@include('common.alert')
<div class="panel panel-default">
    <div class="panel-heading">User Information</div>
    <div class="panel-body">
        <form method="POST" action="/user/{{ $user->id  }}">

            {{ method_field('PUT') }}
            {!! csrf_field() !!}


            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>User Name</label>

                        <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}">
                    </div>
                    <div class="col-md-6">
                        <label>Email</label>

                        <input type="text" class="form-control" name="email" id="email" value="{{ $user->email }}" disabled>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Permission</label>

                        <input type="text" class="form-control" name="permission" id="permission" value="{{ $user->roles->first() ? $user->roles->first()->name : 'No role' }}" disabled>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Created At</label>

                        <input type="text" class="form-control"  value="{{ $user->created_at }}" disabled>
                    </div>
                    <div class="col-md-6">
                        <label>Last Login Time</label>

                        <input type="text" class="form-control"  value="{{ $user->updated_at }}" disabled>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-default pull-right" type="submit">Save changes</button>
            </div>

        </form>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">Activity Log</div>
    <div class="panel-body">
        <div class="table-responsive">

            <table class="table table-hover table-striped">
                <thread>
                    <tr>
                        <td>Log id</td>
                        <td>User name</td>
                        <td>description</td>
                        <td>Log Time</td>
                        <td>IP</td>
                    </tr>
                </thread>

                <tbody>
                @foreach ($logs as $log)
                    <tr>
                        <td>{{$log->id}}</td>
                        <td>{{$log->User->name}}</td>
                        <td>{{$log->description}}</td>
                        <td>{{$log->created_at}}</td>
                        <td>{{$log->ip}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
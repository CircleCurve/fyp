@extends('admin.template')

@section('contentHeader')

    <h1>
        User Management
        <small>edit user information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('user.index') }}"><i class="fa fa-dashboard"></i> User</a></li>
        <li class="active">Edit user</li>
    </ol>

@endsection


@section('content')

    @include('common.alert')


    <div class="row">

        <div class="col-md-3">

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Details</h3>

                    <div class="box-tools">
                        <button data-widget="collapse" class="btn btn-box-tool" type="button"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">

                        <li class="active">
                            <a data-toggle="tab" href="#home">
                                <i class="fa fa-edit"></i> Edit Account Information
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#home2">
                                <i class="fa fa-edit"></i> Edit Account Status
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#home1">
                                <i class="fa  fa-info"></i> Personal Information
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- /.box-body -->
            </div>

        </div>


        <!--<div class="col-md-8 col-md-offset-2">-->
        <div class="tab-content">

            <div id="home" class="tab-pane fade in active">

                <div class="col-md-9">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit an existing user</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('user.update', $user->id) }}">
                            {!! csrf_field() !!}
                            {{ method_field('PUT') }}

                            <div class="box-body">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label>

                                    <input type="text" class="form-control" name="name"
                                           value="{{ old('name')?old('name'):$user->name }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>E-Mail Address</label>
                                    <input type="email" class="form-control" name="email"
                                           value="{{ old('email')?old('email'):$user->email }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="row form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                                    <div class="col-xs-12">
                                        <label>Roles</label>

                                        @if ($errors->has('role'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="hidden_fields_primary" data-name="role">
                                        @foreach($userWithRoles as $userWithRole)
                                            <input type="hidden" class="primary_hidden" name="role[]"
                                                   value="{{ $userWithRole }}">
                                        @endforeach

                                    </div>

                                    @foreach($roles as $role)
                                        <div class="col-sm-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="{{ $role->id }}" class="primary_list"
                                                           data-id="{{ $role->id }}"
                                                           @if (in_array($role->id, $userWithRoles))
                                                           checked
                                                            @endif
                                                    >
                                                    {{ $role->name }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                {{--
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>Permission</label>
                                    </div>

                                    <div data-name="permissions" class="hidden_fields_secondary">
                                        @foreach($roleWithPermissions as $roleWithPermission)
                                            <input type="hidden" class="primary_hidden" name="permissions[]" value="{{ $roleWithPermission }}">
                                        @endforeach
                                    </div>

                                    @foreach($perms as $perm)
                                        <div class="col-sm-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="{{ $perm->id }}" name="permissions_show[]"  data-id="{{$perm->id}}" class="secondary_list"
                                                    @if (in_array($perm->id, $roleWithPermissions))
                                                        checked disabled
                                                     @endif
                                                    >
                                                    {{ $perm->name }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach

                                </div> --}}


                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>Verification</label>
                                        <div class="g-recaptcha"
                                             data-sitekey="6LfU9gMTAAAAAOZZY4UeHQuaLcFUdn9BL0-5wINz"></div>

                                    </div>


                                </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="home2" class="tab-pane fade">

                <div class="col-md-9">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Account Status</h3>
                        </div>
                        <form class ="form-horizontal" role="statusForm" method="POST" action="{{ route('user.status', $user->id) }}">
                            {!! csrf_field() !!}
                            {{ method_field('PUT') }}

                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-xs-2">Account status</label>
                                    <div class="col-xs-10">
                                        <div class="btn-group pull-right" data-toggle="buttons">
                                            @foreach(Config::get('constant.accStatus') as $accStatus => $statusInfo)

                                                @if ($user->status == $accStatus)
                                                        <label class="btn btn-default active">
                                                        <input  type="radio"  name="status" value="{{ $accStatus }}"  checked/> {{ $statusInfo["msg"] }}


                                                        @else
                                                        <label class="btn btn-default">
                                                        <input  type="radio"  name="status" value="{{ $accStatus }}"  /> {{ $statusInfo["msg"] }}

                                                        @endif

                                                        </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <hr />

                                <div class="form-group">
                                    <label class="col-xs-5">Employee status</label>
                                    <div class="col-xs-7">
                                        <div class="btn-group pull-right " data-toggle="buttons">
                                            @foreach(Config::get('constant.employeeStatus') as $employeeStatus => $statusInfo)
                                                @if ($user->employeeStatus == $employeeStatus)
                                                    <label class="btn btn-default active">

                                                @else
                                                    <label class="btn btn-default">
                                                @endif
                                                        <input  type="radio"  name="employeeStatus" value="{{ $employeeStatus }}" autofocus="true"/> {{ $statusInfo["msg"] }}

                                                    </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">

                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div id="home1" class="tab-pane fade">

                <div class="col-md-9">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Personal Information</h3>
                        </div>


                        <form id="createuserform"  method="post" action="{{ route('user.info',  $user->id) }}" >
                            {!! csrf_field() !!}
                            {{ method_field('PUT') }}



                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group{{ $errors->has('firstName') ? ' has-error' : '' }} col-xs-6">
                                        <label>First Name</label>

                                        <input type="text" id="firstName" name="firstName" class="form-control"
                                               placeholder="First Name" value="{{ old('firstName')?old('firstName'):$user->firstName }}">

                                        @if ($errors->has('firstName'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('firstName') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('lastName') ? ' has-error' : '' }} col-xs-6">
                                        <label>Last Name *</label>
                                        <input type="text" id="lastName" name="lastName" class="form-control"
                                               placeholder="Last Name" value="{{ old('lastName')?old('lastName'):$user->lastName }}">
                                        @if ($errors->has('lastName'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('lastName') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="form-group{{ $errors->has('hkid') ? ' has-error' : '' }} col-xs-6">
                                        <label>HKID *</label>
                                        <input type="text" id="hkid" name="hkid" class="form-control"
                                               placeholder="HKID" value="{{ old('hkid')?old('hkid'):$user->hkid }}">

                                        @if ($errors->has('hkid'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('hkid') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group{{ $errors->has('directline') ? ' has-error' : '' }} col-xs-6">
                                        <div class="form-group ">
                                            <label>Direct line *</label>
                                            <input type="text" id="directline" name="directline"
                                                   class="form-control" placeholder="Direct Line" value="{{ old('directline')?old('directline'):$user->directline }}">

                                            @if ($errors->has('directline'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('directline') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} col-xs-12">
                                        <label>Address *</label>
                                            <textarea name="address" class="form-control" rows="1"
                                                      placeholder="Address">{{ old('address')?old('address'):$user->address }}</textarea>
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-xs-4">
                                        <div class="form-group{{ $errors->has('jobTitle') ? ' has-error' : '' }}">
                                            <label>Job Title *</label>
                                            <input type="text" id="jobTitle" name="jobTitle" class="form-control"
                                                   placeholder="Job Title" value="{{ old('jobTitle')?old('jobTitle'):$user->jobTitle }}">
                                            @if ($errors->has('jobTitle'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('jobTitle') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <button id="submitcreateuserform" type="submit" class="btn btn-default"
                                        data-toggle="confirmation" data-btn-ok-label="Confirm!!"
                                        data-btn-ok-icon="glyphicon glyphicon-share-alt"
                                        data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel"
                                        data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                                        data-btn-cancel-class="btn-danger">Update
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('contentJS')
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>
        $(document).ready(function () {


            $(".btn-group > .btn").click(function () {
                $(this).addClass("active").siblings().removeClass("active");
            });

            $('.primary_list').change(function () {
                //var  user_role_permission = {"1":[1,2,3],"2":[2],"3":[2],"4":[3]};
                var user_role_permission = JSON.parse('{!! $user_role_permission !!}');
                var idCurrent = $(this).data('id');


                if ($(this).is(':checked')) {

                    //add hidden field with this value
                    var nameInput = $('.hidden_fields_primary').data('name');
                    var inputToAdd = $('<input type="hidden" class="primary_hidden" name="' + nameInput + '[]" value="' + idCurrent + '">');

                    $('.hidden_fields_primary').append(inputToAdd);

                    $.each(user_role_permission[idCurrent], function (key, value) {
                        //check and disable secondies checkbox
                        $('.secondary_list[value="' + value + '"]').prop("checked", true);
                        $('.secondary_list[value="' + value + '"]').prop("disabled", true);
                        //remove hidden fields with secondary dependency if was setted

                        var hidden = $('.secondary_list_hidden[value="' + value + '"]');
                        if (hidden)
                            hidden.remove();
                    });

                } else {
                    //remove hidden field with this value
                    $('.primary_hidden[value="' + idCurrent + '"]').remove();

                    // uncheck and active secondary checkboxs if are not in other selected primary.

                    var dependencyJson = user_role_permission;

                    var selected = [];
                    $('.primary_hidden').each(function (index, input) {
                        selected.push($(this).val());
                    });

                    $.each(dependencyJson[idCurrent], function (index, secondaryItem) {
                        var ok = 1;

                        $.each(selected, function (index2, selectedItem) {
                            if (dependencyJson[selectedItem].indexOf(secondaryItem) != -1) {
                                ok = 0;
                            }
                        });

                        if (ok) {
                            $('.secondary_list[value="' + secondaryItem + '"]').prop('checked', false);
                            $('.secondary_list[value="' + secondaryItem + '"]').prop('disabled', false);
                        }
                    });

                }
            });


            $('.secondary_list').click(function () {

                var idCurrent = $(this).data('id');
                if ($(this).is(':checked')) {
                    //add hidden field with this value
                    var nameInput = $('.hidden_fields_secondary').data('name');
                    var inputToAdd = $('<input type="hidden" class="secondary_hidden" name="' + nameInput + '[]" value="' + idCurrent + '">');

                    $('.hidden_fields_secondary').append(inputToAdd);

                } else {
                    //remove hidden field with this value
                    $('input.secondary_hidden[value="' + idCurrent + '"]').remove();
                }
            });
        });
    </script>
@endsection
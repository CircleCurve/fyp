     <!-- Modal -->
    <div class="modal fade" id="permission" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Permission List</h4>
                </div>

                <div class="modal-body row">
                    @foreach($roles as $role)
                        <div class="col-xs-4">
                            <div class="input-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="radio"  name="role" value="{{ $role->id }}" style="margin-right: 10px">
                                        {{ $role->display_name }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="modal-footer">
                    <button id="save_tag" type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                </div>
            </div>

        </div>
    </div>

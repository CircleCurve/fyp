@extends('layouts.dashboard')
@section('content')

    @include('common.alert')
    <style type="text/css" xmlns="http://www.w3.org/1999/html">
        .bootstrap-tagsinput .tag [data-role="remove"] { display: none; }

        .bootstrap-tagsinput {
            border: none;
            box-shadow: none;
        }
        .bootstrap-tagsinput input {
            display: none;
        }
    </style>

    <div class="panel panel-default">
        <div class="panel-heading">Register</div>
        <div class="panel-body">
            <form id="reg_user" class="form-horizontal" role="form" method="POST" action="{{ route('user.store') }}">
                {!! csrf_field() !!}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Permission</label>

                    <div class="col-md-6">
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#permission">Select
                        </button>

                        @if ($errors->has('role'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>

                    <div class="col-md-6">
                        <input type="text" value="" id="roles_tag" data-role="tagsinput" disabled>

                    </div>
                </div>

                @include('auth.role')

                <div class="form-group">
                    <label class="col-md-4 control-label"></label>

                    <div class="col-md-6">
                        <div class="g-recaptcha" data-sitekey="6LfU9gMTAAAAAOZZY4UeHQuaLcFUdn9BL0-5wINz"></div>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i>Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('contentJS')
    <script type="text/javascript">
        $(document).ready(function () {

            //console.log($("#roles_tag").tagsinput('items'));

            $('#roles_tag').tagsinput({
                allowDuplicates: false,
                itemValue: 'id',  // this will be used to set id of tag
                itemText: 'label' // this will be used to set text of tag
            });

            $('#save_tag').click(function(){

                $('#roles_tag').tagsinput('removeAll');
                $('#roles_tag').tagsinput('add', $('input[name=role]:checked', '#reg_user').parent().text());
            });

            $('#datetimepicker1').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                //orientation: 'bottom',
                minDate: new Date(),
            });
            $('#datetimepicker2').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                minDate: new Date(),
                //orientation: 'bottom'
            });

            $('.nav li').removeClass('active');

            var $parent = $('#userhref');
            if (!$parent.hasClass('active')) {
                $parent.addClass('active');
            }


        })
    </script>
@endsection
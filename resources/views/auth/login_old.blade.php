@extends('layouts.app')
@section('content')

        <!-- Bootstrap Boilerplate... -->
<div class="panel panel-default">
    <div class="panel-body">
        @if (count($errors) > 0)
            @include('common.error')
        @endif

        <form method="POST" action="/auth/login" class="form-signin">
            {!! csrf_field() !!}
            <h2 class="form-signin-heading">Please sign in</h2>

            <label for="inputEmail" class="sr-only">Email address</label>

            <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email address" required autofocus>

            <label for="inputPassword" class="sr-only">Password</label>

            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>

            <div class="g-recaptcha" data-sitekey="6LfU9gMTAAAAAOZZY4UeHQuaLcFUdn9BL0-5wINz"></div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember"> Remember me
                </label>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>

    </div>
</div>

@endsection
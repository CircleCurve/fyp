@extends('admin.template')

@section('contentHeader')

    <h1>
        User Management
        <small>view all user</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('user.index') }}"><i class="fa fa-dashboard"></i> User</a></li>
        <li class="active">View user</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <a href="{{ route('user.create') }}">
                            <button type="submit" class="btn btn-primary">Add User</button>
                        </a>
                    </h3>
                </div>
                <div class="box-body">
                    <div class="form-group">

                        <div class="row">
                            <!-- Date range -->
                            <div class="col-md-12">
                                <label>Last Login Between:</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="reservation" readonly>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <!-- Date range -->
                            <div class="col-md-12">
                                <button id="clearFilter" class="btn btn-success pull-right">clearFilter</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="filterDataTable" class="table  table-hover table-striped">

                            <thead>
                            <tr>
                                <th>Log id</th>
                                <th>Role</th>
                                <th>User name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Personal Status</th>
                                <th>Create At</th>
                                <th>Last Login</th>
                                <th>Details</th>
                                <th>Remove</th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th class="need">Log id</th>
                                <th class="need">Role</th>
                                <th class="need">User name</th>
                                <th class="need">Email</th>
                                <th class="need">Status</th>
                                <th class="need">Personal Status</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{ $user->roles->first() ? $user->roles->first()->name : 'No role' }}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        <font color="{{ Config::get('constant.accStatus.'.$user->status.'.color') }}">
                                            {{ Config::get('constant.accStatus.'.$user->status.'.msg') }}
                                        </font>
                                    </td>
                                    <td> {{ Config::get('constant.employeeStatus.'. $user->employeeStatus.'.msg') }}</td>
                                    <td>{{$user->created_at}}</td>
                                    <td>{{ isset($user->actionlog[0]) ? $user->actionlog[0]->created_at : ""   }}</td>
                                    <td>
                                        <button type="button" class="btn btn-warning"
                                                onclick="javascript:location.href='/user/{{ $user->id }}'">Detail
                                        </button>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{ route('user.destroy', $user->id) }}">
                                            {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger">Remove</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('contentJS')


    <script>

        //$(function () {
        filterDataTable("filterDataTable", 6);
        //});

    </script>

@endsection
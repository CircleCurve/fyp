@extends('layouts.dashboard')
@section('content')

    @include('common.alert')
    <style type="text/css" xmlns="http://www.w3.org/1999/html">
        /*.bootstrap-tagsinput .tag [data-role="remove"] { display: none; }*/

        .bootstrap-tagsinput {
            border: none;
            box-shadow: none;
        }
        .bootstrap-tagsinput input {
            display: none;
        }
    </style>

    <div class="panel panel-success">
        <div class="panel-heading">Search</div>
        <div class="panel-body">
            <form id="view_user" method="POST" action="{{ route('user.search') }}">

                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>User Name</label>

                            <input type="text" class="form-control" name="userName" id="userName" value="{{ old('userName') }}" placeholder="Please input the part of user name">
                        </div>
                        <div class="col-md-6">
                            <label>Description</label>

                            <input type="text" class="form-control" name="description" id="description" value="{{ old('description') }}" placeholder="Please input the part of description">
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Permission</label>

                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#permission">Select
                            </button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">

                        <div class="col-md-12">
                            <input type="text" value="" id="roles_tag" name="role" data-role="tagsinput">

                        </div>
                    </div>
                </div>


                @include('auth.role')

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Start Date (Only for Today)</label>
                            <div class='input-group date' id='datetimepicker1'>

                                <input type="text" class="form-control" name="startDate" id="startDate" value="{{ old('startDate') }}" readonly>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label>End Date</label>
                            <div class='input-group date' id='datetimepicker2'>

                                <input type="text" class="form-control" name="endDate" id="endDate" value="{{ old('endDate') }}" readonly>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success pull-right" type="submit">Submit</button>
                </div>

            </form>
        </div>

    </div>



    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">User Management</div>

        <!-- Table -->
        <div class="table-responsive">
            <table class="table  table-hover table-striped">
                <thread>
                    <tr>
                        <td>Log id</td>
                        <td>Position</td>
                        <td>User name</td>
                        <td>Email</td>
                        <td>Status</td>
                        <td>Create At</td>
                        <td>Last Login</td>
                        <td>Details</td>
                    </tr>
                </thread>

                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{ $user->roles->first() ? $user->roles->first()->name : 'No role' }}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <font color="{{ Config::get('constant.accStatus.'.$user->status.'.color') }}">
                                {{ Config::get('constant.accStatus.'.$user->status.'.msg') }}
                            </font>
                        </td>
                        <td>{{$user->created_at}}</td>
                        <td>{{$user->updated_at}}</td>
                        <td><button type="button" class="btn btn-warning" onclick="javascript:location.href='/user/{{ $user->id }}'">Detail</button></td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('contentJS')
    <script type="text/javascript">
        $(document).ready(function() {

            $('#roles_tag').tagsinput({

                allowDuplicates: false,
                itemValue: 'id',  // this will be used to set id of tag
                itemText: 'label' // this will be used to set text of tag
            });

            $('#save_tag').click(function(){

                //$('#roles_tag').tagsinput('removeAll');
                $('#roles_tag').tagsinput('add', $('input[name=role]:checked', '#view_user').parent().text());
            });

            $('#datetimepicker1').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                //orientation: 'bottom',
                minDate:new Date(),
            });
            $('#datetimepicker2').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                minDate:new Date(),
                //orientation: 'bottom'
            });

            $('.nav li').removeClass('active');

            var $parent = $('#userhref');
            if (!$parent.hasClass('active')) {
                $parent.addClass('active');
            }
        })
    </script>
@endsection
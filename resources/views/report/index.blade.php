@extends('admin.template')

@section('contentHeader')

    <h1>
        Report
        <small>view report information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('report.index') }}"><i class="fa fa-dashboard"></i> Report</a></li>
        <li class="active">View report</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')

    <div class="row">
        <div class="col-xs-12">

            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

            <table id="datatable" style="display:none">
                <thead>
                <tr>
                    <th></th>
                    <th>Jane</th>
                    <th>John</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Apples</th>
                    <td>3</td>
                    <td>4</td>
                </tr>
                <tr>
                    <th>Pears</th>
                    <td>2</td>
                    <td>0</td>
                </tr>
                <tr>
                    <th>Plums</th>
                    <td>5</td>
                    <td>11</td>
                </tr>
                <tr>
                    <th>Bananas</th>
                    <td>1</td>
                    <td>1</td>
                </tr>
                <tr>
                    <th>Oranges</th>
                    <td>2</td>
                    <td>4</td>
                </tr>
                </tbody>
            </table>


        </div>



    </div>
@endsection

@section('contentJS')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <script>

        $(function () {

            Highcharts.chart('container', {
                data: {
                    table: 'datatable'
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'The amount of symptoms'
                },
                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: 'Units'
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                                this.point.y + ' ' + this.point.name.toLowerCase();
                    }
                }
            });
        });

    </script>

@endsection
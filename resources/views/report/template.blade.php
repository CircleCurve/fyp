@extends('admin.template')

@section('contentHeader')

    <h1>
        Report
        <small>view report information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('report.index') }}"><i class="fa fa-dashboard"></i> Report</a></li>
        <li class="active">View report</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">The amount of symptoms</h3>
                </div>
                <div class="box-body">

                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    <div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


                    <table id="filterDataTable">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Amount</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($symptoms as $symptom)
                            <tr>
                                <th>{{ $symptom->name }}</th>
                                <td>{{ $symptom->amount }}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>


                </div>
            </div>

        </div>


    </div>
@endsection

@section('contentJS')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <script>

        $(function () {
            //filterDataTable("filterDataTable",0);

            highChartTable('container', 'filterDataTable', 'The amount of symptoms (PIE CHART)', 'pie');
            highChartTable('container2', 'filterDataTable', 'The amount of symptoms (LINE CHART)', 'line');
            highChartTable('container3', 'filterDataTable', 'The amount of symptoms (BAR CHART)', 'bar');


            filterDataTable("filterDataTable",0);
        });

    </script>

@endsection
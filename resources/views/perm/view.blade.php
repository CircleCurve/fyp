@extends('admin.template')

@section('contentHeader')

    <h1>
        Permission Management
        <small>view permission information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('perm.index') }}"><i class="fa fa-dashboard"></i> Permission</a></li>
        <li class="active">View permission</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <a href="{{ route('perm.create') }}">
                            <button type="submit" class="btn btn-primary">Add Permission</button>
                        </a>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="roleTable" class="table  table-hover table-striped">

                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Display_name</th>
                                    <th>Linked with</th>
                                    <th>Description</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>

                                @foreach($perms as $perm)
                                    <tr>
                                        <td>{{ $perm->id }}</td>
                                        <td>{{ $perm->name }}</td>
                                        <td>{{ $perm->display_name }}</td>
                                        <td>
                                            @foreach($perm->roles as $role)
                                                {{ $role->name }}
                                            @endforeach
                                        </td>
                                        <td>{{ $perm->description }}</td>
                                        <td>{{ $perm->created_at }}</td>
                                        <td>{{ $perm->updated_at }}</td>
                                        <td><button type="button" class="btn btn-warning" onclick="javascript:location.href='/perm/{{ $perm->id }}'">Detail</button></td>
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>


@endsection

@section('contentJS')
<script>

    $(function () {

        $('#roleTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });


</script>

@endsection

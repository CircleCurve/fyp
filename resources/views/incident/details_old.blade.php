@extends('layouts.dashboard')
@section('content')
    @include('common.alert')

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Information</a></li>
        <li><a data-toggle="tab" href="#menu1">Document</a></li>
        <li><a data-toggle="tab" href="#menu1">Progress</a></li>

    </ul>

    <div class="tab-content" style="margin-top: 20px">

        <div id="home" class="tab-pane fade in active">

            <div class="panel panel-success">
                <div class="panel-heading">Incident details</div>

                <div class="panel-body">
                    <!----create form--->
                    <form id="updateincidentform" action="{{ route('incident.update', $incident->id) }}" data-toggle="validator"
                          method="post">
                        {{ method_field('PUT') }}
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} col-xs-12">
                            <label>Incident Summary</label>
                            <input type="text" id="title" name="title" value="{{ $incident->title }}" class="form-control"
                                   placeholder="Please type incident title" required="" disabled>

                            @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                        </span>
                            @endif

                        </div>

                        <div class="form-group col-xs-12">
                            <div class="row">
                                <div class="col-md-6{{ $errors->has('appId') ? ' has-error' : '' }}">
                                    <label>App ID</label>

                                    <input type="text" class="form-control" name="appId" id="appId"
                                           value="{{ $incident->appId }}" placeholder="Please input an app ID" disabled>

                                    @if ($errors->has('appId'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('appId') }}</strong>
                                </span>
                                    @endif

                                </div>
                                <div class="col-md-6{{ $errors->has('account') ? ' has-error' : '' }} ">
                                    <label>Account</label>

                                    <input type="text" class="form-control" name="account" id="account"
                                           value="{{ $incident->account }}" placeholder="Please input an account" disabled>

                                    @if ($errors->has('account'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('account') }}</strong>
                                </span>

                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('orderNo') ? ' has-error' : '' }} col-xs-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Order number</label>

                                    <input type="text" class="form-control" name="orderNo" id="orderNo"
                                           value="{{ $incident->orderNo }}" placeholder="Please input an order number" disabled>

                                    @if ($errors->has('orderNo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('orderNo') }}</strong>
                                </span>
                                    @endif

                                </div>
                            </div>
                        </div>

                        <div class="form-group col-xs-3">
                            <label>Symptom Selection *</label>
                            <select id="firmWare" name="firmWare" class="form-control" pattern="" data-error=""
                                    required="">
                                <option value="1"> Hardware</option>
                                <option value="2"> Software</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-1">
                            <label></label>
                            <button type="button" class="btn btn-success" style="margin-top:5px;"><span
                                        class="glyphicon glyphicon-share-alt"></span></button>
                        </div>
                        <div class="form-group col-xs-3">
                            <select id="device" name="device" size="3" class="form-control">
                            </select>
                        </div>
                        <div class="form-group col-xs-1">
                            <label></label>
                            <button type="button" class="btn btn-success" style="margin-top:5px;"><span
                                        class="glyphicon glyphicon-share-alt"></span></button>
                        </div>
                        <div class="form-group col-xs-4">
                            <select id="symptom" name="symptom" size="3" class="form-control">

                            </select>
                        </div>
                        <input type="hidden" name="symptoms" id="symptoms" value="{{  $affect }}">

                        <div class="form-group col-xs-12{{ $errors->has('symptoms') ? ' has-error' : '' }}">

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table id="symptomTable" class="table table-bordered table-striped">
                                        <tr class="success topbar">
                                            <td> ID</td>
                                            <td>Firmware</td>
                                            <td>Device</td>
                                            <td>Symptom</td>
                                            <td></td>
                                        </tr>

                                    </table>
                                </div>

                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} col-xs-12">
                            <label>Description</label>
                        <textarea id="description" name="description" class="form-control" rows="2"
                                  required="">{{ $incident->description }}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group col-xs-12">

                            <button id="submitincidentform" type="submit" class="btn btn-default pull-right"
                                    data-toggle="confirmation"
                                    data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt"
                                    data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel"
                                    data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                                    data-btn-cancel-class="btn-danger">
                                Update Incident
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="menu1" class="tab-pane fade">


        </div>

    </div>
@endsection
@section('contentJS')
    <script type="text/javascript">

        $(document).ready(function () {
            var device = JSON.parse('<?= json_encode($issue) ?>');
            var symptom = JSON.parse('<?= json_encode($symptoms) ?>');

            var symptoms = [];
            var symptomIssue = JSON.parse($("#symptoms").val());
            //callback
            var firmWareCallBack = function () {
                $('#symptom').empty();

                firmWare = $("#firmWare").val();
                //$("#firmWare").wrap('<span/>')
                $('#device').empty();
                for (var i = 0; i < device.length; i++) {
                    if (device[i]["mainCategory"] == firmWare) {
                        $('#device')
                                .append($("<option></option>")
                                        .attr("value", device[i]["subCategory"])
                                        .text(device[i]["name"]));
                    }

                }
            };
            firmWareCallBack();

            var deviceCallBack = function () {

                deviceType = $("#device").val();

                //$("#firmWare").wrap('<span/>')
                $('#symptom').empty();
                for (var i = 0; i < symptom.length; i++) {
                    if (symptom[i]["subCategory"] == deviceType) {
                        $('#symptom')
                                .append($("<option></option>")
                                        .attr("value", symptom[i]["id"])
                                        .text(symptom[i]["symptoms"]));
                    }

                }
            };

            $("#firmWare").change(function () {
                        firmWareCallBack();

                    }
            );

            $("#device").change(function () {
                        deviceCallBack();

                    }
            );

            $(".removeSymptom").click(function () {
                console.log($(this).attr('value'));

            });

            var removeSymptomCallBack = function (row) {
                console.log("row value :" + row.attr('value'));
                var value = row.attr('value');
                var removeRow;
                console.log("removeSymptomCallBack");
                for (var i = 0; i < symptoms.length; i++) {

                    if (symptoms[i] === value) {
                        symptoms.splice(i, 1);
                        removeRow = "#symptomRow" + value;
                        $(removeRow).remove();
                        $("#symptoms").val(symptoms);

                    }
                }

            }

            var symptomTableCallBack = function () {
                //$("#symptomTable").find("tr:gt(0)").remove();
                var table = document.getElementById("symptomTable");
                var rowCount = table.getElementsByTagName("tr").length - 1;
                var i = symptoms.length - 1;
                var row, cell;
                row = table.insertRow(rowCount + 1);
                $(row).attr("id", "symptomRow" + symptoms[i]);
                $(row).attr("value", symptoms[i]);

                row.insertCell(0).innerHTML = '<div style="cursor:pointer"  value="' + symptoms[i] + '" class="label label-danger glyphicon glyphicon-trash removeSymptom" data-toggle="confirmation" data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger">Remove</button></td>';
                row.insertCell(0).innerHTML = symptom[(symptoms[i] - 1)]["symptoms"];

                if (deviceType === null){
                    for (var j = 0; j < symptom.length; j++) {
                        deviceStrId = symptom[j]["id"].toString();

                        if (deviceStrId === symptoms[i]) {
                            deviceType = symptom[j]["subCategory"];
                            firmWare = symptom[j]["mainCategory"];
                        }

                    }
                }

                console.log(deviceType);
                var deviceTypeId = "#device option[value='" +  deviceType + "']";
                row.insertCell(0).innerHTML = $(deviceTypeId).text();

                //row.insertCell(0).innerHTML = device[deviceType]["name"];
                var firmWareId = "#firmWare option[value='" + firmWare + "']";
                row.insertCell(0).innerHTML = $(firmWareId).text();

                row.insertCell(0).innerHTML = symptoms[i];

                $(".removeSymptom").click(function () {
                    removeSymptomCallBack($(this));

                });

                deviceType = null ;
                //j = 0;


            };

            function onChange() {
                console.log("onChange");
            }


            $("#symptom").change(function () {

                symptomIssue = [$("#symptom").val()];
                startFilterSymptoms(symptomIssue);
            });
            deviceCallBack();
            startFilterSymptoms(symptomIssue);

            function startFilterSymptoms(symptomIssue) {

               for (var i = 0; i < symptomIssue.length; i++) {
                    if ($.inArray(symptomIssue[i], symptoms) === -1) {
                        symptoms.push(symptomIssue[i]);
                        symptomTableCallBack();
                    }
                }
                $("#symptoms").val(symptoms);
            }
        });
    </script>
@endsection

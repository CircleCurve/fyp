@extends('layouts.dashboard')
@section('content')
    @include('common.alert')

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Create</a></li>
            <li><a data-toggle="tab" href="#menu1">Document</a></li>
        </ul>

    <div class="tab-content" style="margin-top: 20px">

        <div id="home" class="tab-pane fade in active">

            <div class="panel panel-success">
                <div class="panel-heading">Create Incident</div>

                <div class="panel-body">
                    <!----create form--->
                    <form id="createincidentform" action="{{ route('incident.store') }}" data-toggle="validator"
                          method="post">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} col-xs-12">
                            <label>Incident Summary</label>
                            <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control"
                                   placeholder="Please type incident title" required="">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                        </span>
                            @endif

                        </div>

                        <div class="form-group col-xs-12">
                            <div class="row">
                                <div class="col-md-6{{ $errors->has('appId') ? ' has-error' : '' }}">
                                    <label>App ID</label>

                                    <input type="text" class="form-control" name="appId" id="appId"
                                           value="{{ old('appId') }}" placeholder="Please input an app ID">

                                    @if ($errors->has('appId'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('appId') }}</strong>
                                </span>
                                    @endif

                                </div>
                                <div class="col-md-6{{ $errors->has('account') ? ' has-error' : '' }} ">
                                    <label>Account</label>

                                    <input type="text" class="form-control" name="account" id="account"
                                           value="{{ old('account') }}" placeholder="Please input an account">

                                    @if ($errors->has('account'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('account') }}</strong>
                                </span>

                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('orderNo') ? ' has-error' : '' }} col-xs-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Order number</label>

                                    <input type="text" class="form-control" name="orderNo" id="orderNo"
                                           value="{{ old('orderNo') }}" placeholder="Please input an order number">

                                    @if ($errors->has('orderNo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('orderNo') }}</strong>
                                </span>
                                    @endif

                                </div>
                            </div>
                        </div>

                        <div class="form-group col-xs-3">
                            <label>Symptom Selection *</label>
                            <select id="firmWare" name="firmWare" class="form-control" pattern="" data-error=""
                                    required="">
                                <option value="1"> Hardware</option>
                                <option value="2"> Software</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-1">
                            <label></label>
                            <button type="button" class="btn btn-success" style="margin-top:5px;"><span
                                        class="glyphicon glyphicon-share-alt"></span></button>
                        </div>
                        <div class="form-group col-xs-3">
                            <select id="device" name="device" size="3" class="form-control">
                            </select>
                        </div>
                        <div class="form-group col-xs-1">
                            <label></label>
                            <button type="button" class="btn btn-success" style="margin-top:5px;"><span
                                        class="glyphicon glyphicon-share-alt"></span></button>
                        </div>
                        <div class="form-group col-xs-4">
                            <select id="symptom" name="symptom" size="3" class="form-control">

                            </select>
                        </div>
                        <input type="hidden" name="symptoms" id="symptoms">

                        <div class="form-group col-xs-12{{ $errors->has('symptoms') ? ' has-error' : '' }}">

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table id="symptomTable" class="table table-bordered table-striped">
                                        <tr class="success topbar">
                                            <td> ID</td>
                                            <td>Firmware</td>
                                            <td>Device</td>
                                            <td>Symptom</td>
                                            <td></td>
                                        </tr>

                                    </table>
                                </div>

                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} col-xs-12">
                            <label>Description</label>
                        <textarea id="description" name="description" class="form-control" rows="2"
                                  required=""></textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group col-xs-12">

                            <button id="submitincidentform" type="submit" class="btn btn-default pull-right"
                                    data-toggle="confirmation"
                                    data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt"
                                    data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel"
                                    data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                                    data-btn-cancel-class="btn-danger">
                                Create
                                Incident
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="menu1" class="tab-pane fade">


        </div>

    </div>
@endsection
@section('contentJS')
    <script type="text/javascript">

        $(document).ready(function () {
            var device = JSON.parse('<?= json_encode($issue) ?>');
            var symptom = JSON.parse('<?= json_encode($symptoms) ?>');
            var symptoms = [];

            //callback
            var firmWareCallBack = function () {
                $('#symptom').empty();

                firmWare = $("#firmWare").val();
                //$("#firmWare").wrap('<span/>')
                $('#device').empty();
                for (var i = 0; i < device.length; i++) {
                    if (device[i]["mainCategory"] == firmWare) {
                        $('#device')
                                .append($("<option></option>")
                                        .attr("value", device[i]["subCategory"])
                                        .text(device[i]["name"]));
                    }

                }
            };
            firmWareCallBack();

            var deviceCallBack = function () {

                deviceType = $("#device").val();

                console.log("deviceType : " + deviceType);
                //$("#firmWare").wrap('<span/>')
                $('#symptom').empty();
                for (var i = 0; i < symptom.length; i++) {
                    if (symptom[i]["subCategory"] == deviceType) {
                        $('#symptom')
                                .append($("<option></option>")
                                        .attr("value", symptom[i]["id"])
                                        .text(symptom[i]["symptoms"]));
                    }

                }
            };

            $("#firmWare").change(function () {
                        firmWareCallBack();

                    }
            );

            $("#device").change(function () {
                        deviceCallBack();

                    }
            );

            $(".removeSymptom").click(function () {
                console.log($(this).attr('value'));

            });

            var removeSymptomCallBack = function (row) {
                console.log("row value :" + row.attr('value'));
                var value = row.attr('value');
                var removeRow;
                for (var i = 0; i < symptoms.length; i++) {
                    if (symptoms[i] === value) {
                        console.log("value1111");
                        symptoms.splice(i, 1);
                        removeRow = "#symptomRow" + value;
                        $(removeRow).remove();
                    }
                }

            }

            var symptomTableCallBack = function () {

                //$("#symptomTable").find("tr:gt(0)").remove();
                var table = document.getElementById("symptomTable");
                var rowCount = table.getElementsByTagName("tr").length - 1;
                var i = symptoms.length - 1;
                var row, cell;
                console.log("click:" + i + " arr : " + symptoms);
                row = table.insertRow(rowCount + 1);
                $(row).attr("id", "symptomRow" + symptoms[i]);
                $(row).attr("value", symptoms[i]);

                //console.log("i value :" + symptoms[i]);

                row.insertCell(0).innerHTML = '<div style="cursor:pointer"  value="' + symptoms[i] + '" class="label label-danger glyphicon glyphicon-trash removeSymptom" data-toggle="confirmation" data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger">Remove</button></td>';
                row.insertCell(0).innerHTML = symptom[(symptoms[i] - 1)]["symptoms"];

                var deviceTypeId = "#device option[value='" + deviceType + "']";
                row.insertCell(0).innerHTML = $(deviceTypeId).text();

                //row.insertCell(0).innerHTML = device[deviceType]["name"];
                var firmWareId = "#firmWare option[value='" + firmWare + "']";
                row.insertCell(0).innerHTML = $(firmWareId).text();

                row.insertCell(0).innerHTML = symptoms[i];

                $(".removeSymptom").click(function () {
                    removeSymptomCallBack($(this));

                });
                //j = 0;


            };

            function onChange() {
                console.log("onChange");
            }


            $("#symptom").change(function () {

                var symptomIssue = [$("#symptom").val()];
                for (var i = 0; i < symptomIssue.length; i++) {
                    if ($.inArray(symptomIssue[i], symptoms) === -1) {

                        symptoms.push(symptomIssue[i]);
                        symptomTableCallBack();


                    }

                }
                $("#symptoms").val(symptoms);


                //symptomTableCallBack();
                //console.log($("#symptom").val()) ;
            });
        });
    </script>
@endsection

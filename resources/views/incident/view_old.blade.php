@extends('layouts.dashboard')
@section('content')

    @include('common.alert')
    <link rel="stylesheet" href="/fullcalendar/fullcalendar.css" />
    <link rel="stylesheet" href="/fullcalendar/css/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" href="/fullcalendar/ColorPicker/js/jquery.miniColors.css" />

    <style>

        body {
            /*
            margin: 40px 10px;
            padding: 0;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
            font-size: 14px;*/
        }

        #calendar {
            max-width: 900px;
            height:100%;
            margin: 0 auto;
            margin-bottom: 200px ;
        }

    </style>


    <div id="calendar" ></div>

@endsection



@section('contentJS')

    <script type="text/javascript" src="/fullcalendar/js/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript" src="/fullcalendar/lib/moment.min.js"></script>
    <script type="text/javascript" src="/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="/fullcalendar/ColorPicker/js/jquery.miniColors.js"></script>
    <script type="text/javascript" src="/fullcalendar/ui/jquery.ui.position.js"></script>
    <script type="text/javascript" src="/fullcalendar/ui/jquery.ui.resizable.js"></script>
    <script type="text/javascript" src="/fullcalendar/ui/jquery.ui.button.js"></script>
    <script type="text/javascript" src="/fullcalendar/ui/jquery.ui.dialog.js"></script>
    <script type="text/javascript" src="/fullcalendar/gcal.js"></script>

    <script>
        var eventInfo = JSON.parse('{!! json_encode($event) !!}');
        $(document).ready(function () {

            $('#calendar').fullCalendar({
                theme: true,
                googleCalendarApiKey: 'AIzaSyAFHROnSTwFBCseKwh741Z9fJKCrpvGrws',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'

                },
                selectable: true,
                dragOpacity: 0.5,
                weekNumbers: true,
                //weekMode: 'variable',
                editable: true,
                //height: "1000px",

                eventRender: function (event, element) {

                    element.attr('title', event.description);
                },
                eventClick: function(calEvent, jsEvent, view) {
                    //alert ("eventClcik  :" +calEvent.id) ;
                    //location.href="&id="+calEvent.id ;
                    location.href="incident/"+calEvent.id ;
                },
                //defaultDate: '2016-01-12',
                //editable: true,
                //       eventLimit: true, // allow "more" link when too many events
                //events: 'webservice/incident/getIncident.php',
                eventSources: [

                    {
                        googleCalendarId: 'hong_kong__zh_tw@holiday.calendar.google.com',
                        backgroundColor :  "#ff0000"
                    },
                    {
                        googleCalendarId: 'p#weather@group.v.calendar.google.com',
                        backgroundColor :  "#FF0000"
                    },

                ],
                events: eventInfo

            });


        });

    </script>
@endsection
@extends('layouts.dashboard')
@section('content')

    @include('common.alert')


    <div class="panel panel-success">
        <div class="panel-heading">Search incident</div>
        <div class="panel-body">
            <form id="view_user" method="POST" action="{{ route('incident.search') }}">

                {!! csrf_field() !!}

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Incident Id</label>

                            <input type="text" class="form-control" name="id" id="id" value="{{ old('id') }}"
                                   placeholder="Please input an id of incident">

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Incident Summary</label>

                            <input type="text" class="form-control" name="title" id="title"
                                   value="{{ old('title') }}"
                                   placeholder="Please input the part of incident summary">
                        </div>
                        <div class="col-md-6">
                            <label>Description</label>

                            <input type="text" class="form-control" name="description" id="description"
                                   value="{{ old('description') }}"
                                   placeholder="Please input the part of description">
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>From</label>

                            <input type="text" class="form-control" name="from" id="from"
                                   value="{{ old('from') }}"
                                   placeholder="Please input who assign the case">
                        </div>
                        <div class="col-md-6">
                            <label>Assign To</label>

                            <input type="text" class="form-control" name="to" id="to" value="{{ old('to') }}"
                                   placeholder="Please input who is being assigned">
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Start Date (Only for Today)</label>

                            <div class='input-group date' id='datetimepicker1'>

                                <input type="text" class="form-control" name="startDate" id="startDate"
                                       value="{{ old('startDate') }}" readonly>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label>End Date</label>

                            <div class='input-group date' id='datetimepicker2'>

                                <input type="text" class="form-control" name="endDate" id="endDate"
                                       value="{{ old('endDate') }}" readonly>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success pull-right" type="submit">Submit</button>
                </div>

            </form>
        </div>
    </div>
    <ul class="nav nav-tabs" style="margin-top: 20px;">
        <li class="active"><a data-toggle="tab" href="#home">Assign</a></li>
        <li><a data-toggle="tab" href="#menu1">Received</a></li>
        <li><a data-toggle="tab" href="#menu2">Search result</a></li>

    </ul>

    <div class="tab-content" style="margin-top: 20px">

        <div id="home" class="tab-pane fade in active">

            <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">Incident information</div>

                <!-- Table -->
                <div class="table-responsive">
                    <table class="table  table-hover table-striped">
                        <thread>
                            <tr>
                                <td>Incident ID</td>
                                <td>Summary</td>
                                <td>From</td>
                                <td>Assign to</td>
                                <td>Create At</td>
                                <td>Update At</td>
                                <td>Details</td>
                            </tr>
                        </thread>

                        <tbody>

                        @foreach($assignIncidents as $user)
                            @foreach($user->incident as $incident)
                                <tr>
                                    <td>{{ $incident->id }}</td>
                                    <td>{{ $incident->title }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $incident->calendar->user->name }}</td>
                                    <td>{{ $incident->created_at }}</td>
                                    <td>{{ $incident->updated_at }}</td>
                                    <td>
                                        <button type="button" class="btn btn-warning"
                                                onclick="javascript:location.href='/incident/{{ $incident->id }}'">
                                            Detail
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="menu1" class="tab-pane fade">

            <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">Incident information</div>

                <!-- Table -->
                <div class="table-responsive">
                    <table class="table  table-hover table-striped">
                        <thread>
                            <tr>
                                <td>Incident ID</td>
                                <td>Summary</td>
                                <td>From</td>
                                <td>Assign to</td>
                                <td>Create At</td>
                                <td>Update At</td>
                                <td>Expected endtime</td>
                                <td>Details</td>
                            </tr>
                        </thread>

                        <tbody>
                        @foreach($receiveIncidents as $user)
                            @foreach($user->calendar as $calendar)
                                <tr>
                                    <td>{{ $calendar->incident->id }}</td>
                                    <td>{{ $calendar->incident->title }}</td>
                                    <td>{{ $calendar->incident->user->name }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $calendar->incident->created_at }}</td>
                                    <td>{{ $calendar->incident->updated_at }}</td>
                                    <td>{{ $calendar->end }}</td>
                                    <td>
                                        <button type="button" class="btn btn-warning"
                                                onclick="javascript:location.href='/incident/{{ $calendar->incident->id }}'">
                                            Detail
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="menu2" class="tab-pane fade">
            <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">Incident information</div>

                <!-- Table -->
                <div class="table-responsive">
                    <table class="table  table-hover table-striped">
                        <thread>
                            <tr>
                                <td>Incident ID</td>
                                <td>Summary</td>
                                <td>From</td>
                                <td>Assign to</td>
                                <td>Create At</td>
                                <td>Update At</td>
                                <td>Expected endtime</td>
                                <td>Details</td>
                            </tr>
                        </thread>

                        <tbody>
                        @foreach($searchIncidents as $calendar)
                                <tr>
                                    <td>{{ $calendar->id }}</td>
                                    <td>{{ $calendar->title }}</td>
                                    <td>{{ $calendar->end }}</td>
                                    <td>
                                        <button type="button" class="btn btn-warning"
                                                onclick="javascript:location.href='/incident/{{ $calendar->id }}'">
                                            Detail
                                        </button>
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('contentJS')
    <script type="text/javascript">
        $(document).ready(function () {


            $('#datetimepicker1').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                //orientation: 'bottom',
                minDate: new Date(),
            });
            $('#datetimepicker2').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                minDate: new Date(),
                //orientation: 'bottom'
            });
            /*
             $('.nav li').removeClass('active');

             var $parent = $('#userhref');
             if (!$parent.hasClass('active')) {
             $parent.addClass('active');
             }*/
        })
    </script>
@endsection
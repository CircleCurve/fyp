@extends('admin.template')

@section('contentHeader')

    <h1>
        Incident Management
        <small>view assigned and received incident</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('incident.index') }}"><i class="fa fa-dashboard"></i> Incident</a></li>
        <li class="active">View incident</li>
    </ol>

@endsection

@section('content')

    @include('common.alert')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <a href="{{ route('incident.create') }}">
                            <button type="submit" class="btn btn-primary">Add Incident</button>
                        </a>
                    </h3>
                </div>



                <div class="box-body">

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#received">Received</a></li>
                        <li><a data-toggle="tab" href="#assigned">Assign</a></li>

                    </ul>

                </div>
                <!-- /.box-header -->
                <div class="tab-content" style="margin-top: 20px">
                    <div id="assigned" class="tab-pane fade in ">

                        <div class="box-body">
                            <div class="form-group">

                                <div class="row">
                                    <!-- Date range -->
                                    <div class="col-md-12">
                                        <label>Create Between:</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="reservation"  readonly>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <!-- Date range -->
                                    <div class="col-md-12">
                                        <button id="clearFilter" class="btn btn-success pull-right">clearFilter</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="filterDataTable" class="table  table-hover table-striped">

                                    <thead>
                                    <tr>
                                        <th>Incident ID</th>
                                        <th>Summary</th>
                                        <th>From</th>
                                        <th>Assign to</th>
                                        <th>Current progress</th>
                                        <th>Create At</th>
                                        <th>Update At</th>
                                        <th>Details</th>
                                        <th>Reassign</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th class="need">Incident ID</th>
                                        <th class="need">Summary</th>
                                        <th class="need">From</th>
                                        <th class="need">Assign to</th>
                                        <th class="need">Current progress</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($assignIncidents as $user)
                                        @foreach($user->incident as $incident)

                                            <tr>
                                                <td>{{ $incident->id }}</td>
                                                <td>{{ $incident->title }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $incident->calendar->user->name }}</td>
                                                <td>
                                                @if ( isset ($incident->calendar->progress[0]))
                                                    {{ $incident->calendar->progress[0]->name }}
                                                @endif
                                                </td>
                                                <td>{{ $incident->created_at }}</td>
                                                <td>{{ $incident->updated_at }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-warning"
                                                            onclick="javascript:location.href='/incident/{{ $incident->id }}'">
                                                        Detail
                                                    </button>
                                                </td>
                                                <td>
                                                    <form method="POST" action="{{ route('incident.reassign', $incident->id) }}">
                                                        {!! csrf_field() !!}
                                                        {{ method_field('PUT') }}

                                                        <button type="submit" class="btn btn-primary">
                                                            Reassign
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

                    <div id="received" class="tab-pane fade in active">

                        <div class="box-body">
                            <div class="form-group">

                                <div class="row">
                                    <!-- Date range -->
                                    <div class="col-md-12">
                                        <label>Create Between:</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="reservation1"  readonly>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <!-- Date range -->
                                    <div class="col-md-12">
                                        <button id="clearFilter" class="btn btn-success pull-right">clearFilter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="filterDataTable1" class="table  table-hover table-striped">

                                    <thead>
                                    <tr>
                                        <th>Incident ID</th>
                                        <th>Summary</th>
                                        <th>From</th>
                                        <th>Assign to</th>
                                        <th>Current progress</th>
                                        <th>Create At</th>
                                        <th>Update At</th>
                                        <th>Expected endtime</th>
                                        <th>Details</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th class="need">Incident ID</th>
                                        <th class="need">Summary</th>
                                        <th class="need">From</th>
                                        <th class="need">Assign to</th>
                                        <th class="need">Current progress</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($receiveIncidents as $user)
                                        @foreach($user->calendar as $calendar)
                                            <tr>
                                                <td>{{ $calendar->incident->id }}</td>
                                                <td>{{ $calendar->incident->title }}</td>
                                                <td>{{ $calendar->incident->user->name }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>
                                                    @if ( isset ($calendar->incident->progress[0]))
                                                        {{ $calendar->incident->progress[0]->name }}
                                                    @endif
                                                </td>
                                                <td>{{ $calendar->incident->created_at }}</td>
                                                <td>{{ $calendar->incident->updated_at }}</td>
                                                <td>{{ $calendar->end }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-warning"
                                                            onclick="javascript:location.href='/incident/{{ $calendar->incident->id }}'">
                                                        Detail
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                <!-- /.box-body -->
                </div>
            </div>
            <!-- /.box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('contentJS')


    <script>

        //$(function () {
        filterDataTable("filterDataTable",4);
        filterDataTable("filterDataTable1",4, "reservation1");

        //});

    </script>

@endsection
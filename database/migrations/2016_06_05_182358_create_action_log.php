<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actionlog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->index();
            $table->string("type","10");
            $table->text("description");
            $table->timestamps();
            $table->string("ip", 40);
            $table->json("extra") ; 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actionlog');
    }
}

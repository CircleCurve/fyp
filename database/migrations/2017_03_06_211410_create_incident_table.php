<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('incident', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 50);
            $table->integer('appId')->index();
            $table->string('account',50);
            $table->string('orderNo',50);
            $table->integer('status')->index();
            $table->text('description')->nullable();
            $table->integer('user_id')->index();
            $table->string("ip", 40);
            $table->timestamp('completed_time');
            $table->timestamp('expected_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('incident');

    }
}

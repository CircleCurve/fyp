<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('users', function (Blueprint $table) {
            $table->string("firstName", 30);
            $table->string("lastName", 30);
            $table->integer('sex');
            $table->string('hkid', 8);
            $table->dateTime('birthday');
            $table->integer('directline');
            $table->text('address');
            $table->string('jobTitle', 50);
            $table->integer('employeeStatus');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("firstName");
            $table->dropColumn("lastName");
            $table->dropColumn('sex');
            $table->dropColumn('hkid');
            $table->dropColumn('birthday');
            $table->dropColumn('directline');
            $table->dropColumn('address');
            $table->dropColumn('jobTitle');


        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSymptomsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('symptoms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mainCategory')->index();
            $table->integer('subCategory')->index();
            $table->string('symptoms', 50);
            $table->integer('status')->index();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('symptoms');

    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentActionlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('incident_progress', function (Blueprint $table) {
            $table->integer("user_id")->index();
            $table->text("title");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('incident_progress', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('title');
        });
    }
}

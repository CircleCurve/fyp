<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::create('calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('incident_id')->index();
            $table->integer('user_id')->index();
            $table->string('title', 50);
            $table->timestamp('start');
            $table->timestamp('end');
            $table->integer('allDay')->default(0);
            $table->string('color',50)->nullable();
            $table->string('repeatedevent',50)->nullable();
            $table->text('description')->nullable();
            $table->string('reminder',50)->nullable();
            $table->string('timer',50)->nullable();
            $table->string('remind_number',50)->nullable();
            $table->integer('public_own');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendar');
    }
}

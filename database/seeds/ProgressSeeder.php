<?php

use Illuminate\Database\Seeder;
use App\Progress;
class ProgressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Progress::create([
            'name' => "Assigned",
            "color" => "#378006",
            'description' => "",
        ]);

        Progress::create([
            'name' => "completed",
            "color" => "#78B4B4",
            'description' => "",
        ]);

        Progress::create([
            'name' => "In Review",
            "color" => "#ED220C",
            'description' => "",
        ]);

        Progress::create([
            'name' => "Communicating with front end staff",
            "color" => "#32D20B",
            'description' => "",
        ]);

        Progress::create([
            'name' => "closed",
            "color" => "#E87605",
            'description' => "",
        ]);
    }
}

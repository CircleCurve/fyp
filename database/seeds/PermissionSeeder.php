<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
use App\Permission;
class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Administrator'; // optional
        $admin->description  = 'Admin can do anything in the system'; // optional
        $admin->save();
        */

        //
        /*
        $admin = new Role();
        $admin->name         = 'developer';
        $admin->display_name = 'Developer'; // optional
        $admin->description  = 'Developer can receive an incident and response with it'; // optional
        $admin->save();

        $admin = new Role();
        $admin->name         = 'front-end';
        $admin->display_name = 'Front-end staff'; // optional
        $admin->description  = 'Front-end staff can send out and view incident'; // optional
        $admin->save();

        $admin = new Role();
        $admin->name         = 'analysis';
        $admin->display_name = 'Analysis'; // optional
        $admin->description  = 'User can view report and analysis'; // optional
        $admin->save();
        
        */
        /*
        $user = User::where('name', '=', 'patrick')->first();

// role attach alias
        $user->attachRole(1); // parameter can be an Role object, array, or id
        */

        //$admin = new Role();

        
        $editUser = new Permission();
        $editUser->name         = 'add-user';
        $editUser->display_name = 'Add Users'; // optional
// Allow a user to...
        $editUser->description  = 'add users'; // optional
        $editUser->save();
        Role::find(1)->attachPermission(["id" => 2]);
        //$admin->attachPermission(["id" => 1]);;




    }
}

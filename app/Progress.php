<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progress extends Model
{
    //
    protected $table = "progress";
    protected $touches = ['incidents'];

    public function user()
    {
        return $this->belongsToMany('App\User', 'incident_progress', 'progress_id', 'user_id')->withTimestamps();
    }

    public function incidents()
    {
        return $this->belongsToMany('App\Incident')->withTimestamps();
    }
}

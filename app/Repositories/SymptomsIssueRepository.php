<?php
/**
 * Created by PhpStorm.
 * User: patrick
 * Date: 17年1月16日
 * Time: 上午1:53
 */

namespace App\Repositories;

use App\Symptoms;
use App\SymptomsIssue;
use Auth;
use Config;
use Request;

class SymptomsIssueRepository
{
    public function add ($incidentId, $symptomsId){
        $filterSymptomsId = explode(',', $symptomsId);
        $count = count($filterSymptomsId);
        for ($i = 0 ; $i< $count ; $i++){
            SymptomsIssue::create([
                'incident_id' => $incidentId,
                'symptoms_id' => $filterSymptomsId[$i],
                'status' => 1
            ]);
        }
    }


}
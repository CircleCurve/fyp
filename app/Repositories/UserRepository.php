<?php
/**
 * Created by PhpStorm.
 * User: patrick
 * Date: 17年1月16日
 * Time: 上午1:53
 */

namespace App\Repositories;

use App\Role;
use App\User;
use DB;

class UserRepository
{

    public function search( $request)
    {

        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $userName = $request->get('userName');
        $email = $request->get('email');
        $role = $request->get('role');

        $users = User::query();

        if (!empty($userName)) {

            $users->where('name', 'like', '%'.$userName.'%');
        }

        if (!empty($email)) {
            $users->where('email', 'like', '%'.$email.'%');
        }

        if (!empty($startDate)){
            $users->where('created_at', '>=', $startDate);

        }
        if (!empty($endDate)){
            $users->where('created_at', '<', $endDate);
        }

        if (!empty($role)){
            $users->whereHas('roles', function($query) use ($role){
                $query->where('id', $role);
            });
            //$role = Role::find(1)->users()->get();
        }
        //$users = $users->orderBy('created_at','desc');
        //$users->where('name', 'like', '%'.$userName.'%');
        $users->orderBy('created_at','desc');


        return $users;
    }

}
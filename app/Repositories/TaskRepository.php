<?php
/**
 * Created by PhpStorm.
 * User: patrick
 * Date: 16年5月28日
 * Time: 下午11:22
 */
namespace App\Repositories;

use App\User;
use App\Task;

class TaskRepository{



    public function getTask(User $user){

        $clause = ['user_id'=>$user->id, ];
        $tasks =  Task::where ($clause)
            ->get();
        return $tasks;

    }

    public function forUser(User $user){

         return Task::where('user_id', $user->id)
            ->orderBy('created_at','asc')
            ->get();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: patrick
 * Date: 16年5月28日
 * Time: 下午11:22
 */
namespace App\Repositories;


use App\Symptoms;
use App\SymptomsIssue;
use DB;

class SymptomsRepository{

    public function getIssue() {


        $issue = DB::table('symptoms as a')
            ->select('a.mainCategory','a.subCategory as subCategory','b.name')
            ->leftJoin('issue as b', 'a.subCategory', '=', 'b.id')
            ->groupBy('a.subCategory')
            ->get();

        return $issue;
    }

    public function getSymptoms(){
        $symptoms = DB::table('symptoms')
            ->get();
        return $symptoms;
    }

    public function getSymptomsAmount() {

        $symptoms = DB::table('symptoms as a')
            ->select('a.id as symptoms_id', 'a.symptoms as name', DB::raw('count(1) as amount'))
            ->leftJoin('symptomsIssue as b', 'a.id', '=', 'b.symptoms_id')
            ->where ('b.symptoms_id', '!=' ,'null')
            ->groupBy('b.symptoms_id')
            ->get();

        return $symptoms;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: patrick
 * Date: 17年1月16日
 * Time: 上午1:53
 */

namespace App\Repositories;

use App\User;
use Auth;
use Config;
use App\Actionlog;
use Request;

class ActionLogRepository
{
    public function add ($type, $replacement = ""){
        $description = Config::get('constant.actionlog.'.$type.'.msg');
        if ($replacement != "") {
            $description = sprintf($description, $replacement);
        }

        Actionlog::create([
            'user_id' => Auth::user()->id,
            'type' => Config::get('constant.actionlog.'.$type.'.id'),
            'description' => $description,
            'ip' => Request::ip(),
            'extra' => json_encode([])
        ]);
    }

    public function search( $request)
    {

        $description = $request->get('description');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $userName = $request->get('userName');

        $logs = Actionlog::whereHas('User' , function ($query) use ($userName) {
            if (!empty($userName)){
                $query->where('name', 'like', '%'.$userName.'%');
            }
        });

        if (!empty($description)) {
            $logs->where('description', 'like', '%'.$description.'%');
        }

        if (!empty($startDate)){
            $logs->where('created_at', '>=', $startDate);

        }
        if (!empty($endDate)){
            $logs->where('created_at', '<', $endDate);

        }
        $logs = $logs->orderBy('created_at','desc');


        return $logs;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: kwannokng
 * Date: 9/3/2017
 * Time: 12:02 AM
 */

namespace App\Repositories;


use App\Calendar;
use Illuminate\Http\Request;
use Config;
use DB;
use App\User;
class CalendarRepository
{

    public function add(Request $request, $incidentId, $userId){

        $title = $request->get('title');
        $description = $request->get('description');
        $start = date("Y-m-d H:i:s" , mktime(date('H'),date('i'),date('s'),date("m"),date("d"),date("Y")) );
        $end = date("Y-m-d H:i:s" , mktime(date('H'),date('i'),date('s'),date("m"),date("d")+3,date("Y")) );

        Calendar::create([
            'title' => $title,
            'incident_id'=>$incidentId,
            'user_id'=>$userId,
            'start'=>$start,
            'end'=>$end,
            'color'=>Config::get('constant.calendar.setting.1.color'),
            'description'=>$description,
            'public_own'=>'1'
        ]);


    }

    public function reassign($calendarId, $userId){
        Calendar::where('id', $calendarId)->update([
            'user_id' => $userId
        ]);

    }


}
<?php
/**
 * Created by PhpStorm.
 * User: kwannokng
 * Date: 9/3/2017
 * Time: 12:13 AM
 */

namespace App\Repositories;

use App\Calendar;
use App\Incident;
use App\User;
use DB;
use Auth;

class IncidentRepository
{


    public function lessIncidentFirst()
    {
        $query = 'SELECT a.id, a.name, IFNULL( b.user_id, 0 ) uid,c.status incdentStatus, ' .
            'CASE WHEN b.user_id IS NULL THEN  "0" ELSE COUNT( 1 ) END amount ' .
            'FROM users a ' .
            'LEFT JOIN calendar b ON a.id = b.user_id '
            . 'LEFT JOIN incident c on c.id = b.incident_id '
            . 'WHERE a.id !='.Auth::user()->id
            . ' AND a.employeeStatus =1'
            . ' GROUP BY a.id '
            . 'HAVING incdentStatus IS NULL OR incdentStatus !=1 '
            . 'ORDER BY amount, a.id '
            . ' limit 1  ';

        $result = DB::select($query);

        if (empty($result)) {
            return $this->roundRobin();
        }

        return $result;
    }


    public function roundRobin()
    {
        $query = 'SELECT a.id, a.name, IFNULL( b.user_id, 0 ) uid,' .
            'CASE WHEN b.user_id IS NULL THEN  "0" ELSE COUNT( 1 ) END amount ' .
            'FROM users a '
            .'LEFT JOIN calendar b ON a.id = b.user_id '
            .'WHERE a.id !='.Auth::user()->id
            . ' AND a.employeeStatus =1'
            . ' GROUP BY a.id '
            . 'ORDER BY amount, a.id '
            . 'limit 1 ';

        $result = DB::select($query);

        //dd($result);
        return $result;
    }

    public static function globalAssign($userId, $type = "assign")
    {

        switch ($type) {
            case "assign":
                $incidents = User::where('id', $userId)->with(['incident.calendar.progress.user'])->get();
                break;

            case "isAssigned":
                $incidents = User::where('id', $userId)->with(['calendar.incident.progress.user'])->get();
                break;


        }
    }


    public function assign($userId, $type = "assign")
    {

        switch ($type) {
            case "assign":
                $incidents = User::where('id', $userId)->with(['incident.calendar.progress.user', 'incident.calendar.progress'=>function($q){
                    $q->orderBy('pivot_created_at', 'desc');
                }])->get();

                break ;

            case "isAssigned":
                $incidents = User::where('id', $userId)->with(['calendar.incident.progress.user','calendar.incident.progress'=>function($q){
                $q->orderBy('pivot_created_at', 'desc');
                }])->get();


                break;

            case "stillValid":
                $users = User::where('id', $userId)->with(['calendar.incident.progress.user','calendar.incident.progress'=>function($q) {
                    $q->orderBy('pivot_created_at', 'desc');
                }])->get();

                foreach ($users as $user){
                    foreach ($user->calendar as $key=> $calendar){
                            if (!isset($calendar->incident->progress[0])){
                                continue;
                            }
                            $status = $calendar->incident->progress[0]->name;
                            if ($status == "completed"){
                                unset($user->calendar[$key]);
                            
                        }
                    }
                }
                $incidents = $users;

            break ;
            case "stillValidAssign":
                $users = User::where('id', $userId)->with(['incident.calendar.progress.user', 'incident.calendar.progress'=>function($q){
                    $q->orderBy('pivot_created_at', 'desc');
                }])->get();

                foreach ($users as $user){
                    foreach ($user->incident as $key=> $incident){
                        if (!isset($incident->calendar->progress[0])){
                            continue;
                        }

                        $status = $incident->calendar->progress[0]->name;
                        if ($status == "completed"){
                            unset($user->incident[$key]);

                        }
                    }
                }
                $incidents = $users;

             break;

        }

        return $incidents;

        /*

        switch ($type) {
            case "assign":
                $incidentM = new Incident();
                $incidentM = $incidentM->where('user_id', $userId);
                $incidents = $incidentM->with('calendar')->orderBy('updated_at', 'desc')->get();

                $table = 'calendar';
                break;

            case "isAssigned":
                $incidentM = new Calendar();
                $incidentM = $incidentM->where('user_id', $userId);
                $incidents = $incidentM->with('incident')->orderBy('updated_at', 'desc')->get();

                $table = 'incident';
                break;

        }

        $ids = [];
        foreach ($incidents as $incident) {
            if (!in_array($incident->user_id, $ids, true)) {
                array_push($ids, $incident->user_id);
            }

            if (!in_array($incident->{$table}->user_id, $ids, true)) {
                array_push($ids, $incident->{$table}->user_id);
            }
        }

        $userInfos = User::whereIn('id', $ids)->get();

        foreach ($incidents as $incident) {
            foreach ($userInfos as $userInfo) {
                if ($userInfo->id == $incident->user_id) {
                    $incident->userName = $userInfo->name;

                }

                if ($userInfo->id == $incident->{$table}->user_id) {
                    $incident->{$table}->userName = $userInfo->name;
                }
            }
        }

        return $incidents;*/
    }

    public function search( $request)
    {

        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $incident_id = $request->get('id');
        $title = $request->get('title');
        $from = $request->get('from');
        $to = $request->get('to');

        $incident = Calendar::query();
        //$incident->where('user_id', '=', Auth::user()->id);

        if (!empty($incident_id)) {
            $incident->where('id', '=', $incident_id);
        }

        if (!empty($title)) {
            $incident->where('title', 'like', '%'.$title.'%');
        }
        /*
        if (!empty($from)) {
            $userIds = User::where('name', $from)->lists('id')->toArray();
            $incident->whereIn('user_id', $userIds);
        }*/

        if (!empty($to)) {


            $userIds = User::where('name', $to)->lists('id')->toArray();

            $incident->whereIn('user_id', $userIds);
        }

        /*
        if (!empty($to)) {
            $incident->where('title', 'like', '%'.$title.'%');
        }

        if (!empty($startDate)){
            $incident->where('created_at', '>=', $startDate);

        }
        if (!empty($endDate)){
            $incident->where('created_at', '<', $endDate);
        }*/

        //$users = $users->orderBy('created_at','desc');
        //$users->where('name', 'like', '%'.$userName.'%');
        $incident->orderBy('updated_at','desc');


        return $incident;
    }


}
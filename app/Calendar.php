<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    //
    protected $table = "calendar";

    protected $fillable = ['title', 'incident_id', 'user_id', 'title','start','end','color', 'allDay','repeatedevent', 'description', 'reminder', 'timer', 'remind_number', 'public_own'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function incident()
    {
        return $this->belongsTo('App\Incident', 'incident_id');
    }

    public function progress()
    {
        return $this->belongsToMany('App\Progress', 'incident_progress', 'incident_id', 'progress_id')->withTimestamps()->withPivot('title', 'description');
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Symptoms extends Model
{
    //
    protected $table = 'symptoms';

    protected $fillable = ['mainCategory', 'subCategory', 'symptoms', 'status'];


    public function symptomsIssue(){
        return $this->hasMany('App\SymptomsIssue');
    }

    public function symptomsIssues(){
        return $this->belongsToMany('App\Incident', 'symptomsIssue', 'symptoms_id', 'incident_id')->withTimestamps()->withPivot('title', 'description');

    }
}

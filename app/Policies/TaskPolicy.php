<?php

namespace App\Policies;

use App\Repositories\TaskRepository;
use App\Task;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine destroy id is same as User_id
     * @param User $user
     * @param Task $task
     * @return bool
     */
    public function destroy(User $user, Task $task){

        return $user->id === $task->user_id ;
    }
}

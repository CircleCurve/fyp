<?php

namespace App\Http\Middleware;

use Closure;

class CloudFlareProxies
{
    /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
   private $ip = [
       '103.21.244.0/22',
        '103.22.200.0/22',
        '103.31.4.0/22',
        '104.16.0.0/12',
        '108.162.192.0/18',
        '131.0.72.0/22',
        '141.101.64.0/18',
        '162.158.0.0/15',
        '172.64.0.0/13',
        '173.245.48.0/20',
        '188.114.96.0/20',
        '190.93.240.0/20',
        '197.234.240.0/22',
        '198.41.128.0/17',
        '199.27.128.0/21',
   ]; 


  public function handle($request, Closure $next) {
      /*
    $proxy_ips = \Cache::remember('cloudFlareProxyIps', 1440, function () {
      $url = 'https://www.cloudflare.com/ips-v4';
      $ips = file_get_contents($url);
      return array_filter(explode("\n", $ips));
    });*/
    
    $proxy_ips = $this->ip; 

    if ( $forwarded_for = $request->headers->get('X_FORWARDED_FOR') ) {
      $forwarded_ips = explode(", ", $forwarded_for);
      foreach ( $forwarded_ips as $forwarded_ip ) {
        if ( \Symfony\Component\HttpFoundation\IpUtils::checkIp($forwarded_ip, $proxy_ips) ) {
          $proxy_ips[] = $request->server->get('REMOTE_ADDR');
          break;
        }
      }
    }

    $request->setTrustedProxies($proxy_ips);

    return $next($request);
}
}

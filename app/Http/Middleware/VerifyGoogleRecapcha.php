<?php
namespace App\Http\Middleware;


use App\Libraries\CrossPlatform;
use Closure;


class VerifyGoogleRecapcha {
    const recapcha = "https://www.google.com/recaptcha/api/siteverify" ;
    const secret = "6LfU9gMTAAAAAHm705CLR5gNuvKxkK23SoR_yQPS";

    public function __construct()
    {


    }

    public function handle($request, Closure $next) {

        $crossPlatform = new CrossPlatform();

        $validator=  \Validator::make($request->all(),[
            "g-recaptcha-response" => "required|string",
        ],[
            "g-recaptcha-response.required"=>"Please accept Repcapcha Verification",
            "g-recaptcha-response.string"=>"Recapcha Validation Error (Internal Checking)"

        ]);
        if ($validator->fails()){
            $request->session()->flash('alert-danger', 'Please accept Repcapcha Verification');

            return back()->withInput()->withErrors($validator->errors()->all());
        }

        $rtn = $crossPlatform->post_url_contents(self::recapcha, [
            "response" =>$request->get("g-recaptcha-response"),
            "secret" => self::secret
        ]);

        $rtn = json_decode($rtn, true) ;

        if ($rtn["success"] === true){

            if ($rtn["hostname"] == $_SERVER['HTTP_HOST']){
                return $next($request) ;
            }
        }

        $request->session()->flash('alert-danger', 'Recapcha Validation Error ');


        return back()->withInput()->withErrors(["Recapcha Validation Error "]);



    }


}

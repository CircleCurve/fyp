<?php

namespace App\Http\Middleware;

use Closure;

class UserStatusMiddleware
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $user = $this->auth->getUser();
        if ($user->status != 1){
            return back()
                ->withInput($request->only($this->loginUsername(), 'remember'))
                ->withErrors([
                    $this->loginUsername() => "Your account has been banned. Please contact an Administrator",
                ]);
        }


        return $next($request);
    }
}

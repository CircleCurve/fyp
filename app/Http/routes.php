<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['auth','checkperms']], function () {


    Route::resource('/log', 'Log\ActionlogController');
    //Route::post('/log', 'Log\ActionlogController@index')->name('log.');

    //Route::get('/user/search', 'Auth\UserController@search');
    Route::post('/user/search', 'Auth\UserController@index')->name('user.search');

    Route::put('/user/info/{id}', 'Auth\UserController@updatePersonalInformation')->name('user.info');

    Route::put('/user/status/{id}', 'Auth\UserController@postStatus')->name('user.status');

    Route::resource('/user', 'Auth\UserController');
    Route::resource('/role', 'Auth\RoleController');
    Route::resource('/perm', 'Auth\PermissionController');


    Route::put('/incident/reassign/{id}', 'Incident\IncidentController@reassign')->name('incident.reassign');
    Route::get('/incident/assigned', 'Incident\IncidentController@assigned')->name('incident.assigned');
    Route::post('/incident/search', 'Incident\IncidentController@search')->name('incident.search');
    Route::put('/incident/progress/{id}', 'Incident\IncidentController@updateProgress')->name('incident.progress');

    Route::resource('/incident', 'Incident\IncidentController');
    Route::resource('/report', 'Report\ReportController');


    // Registration Routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister')->name('auth.register');
    Route::post('auth/register', 'Auth\AuthController@postRegister')->name('auth.register');

});

Route::group(['middleware' => ['auth']], function() {
    Route::get('/', function () {
        //return view ('layouts.dashboard');
        return view('admin.index');
    });
    
});

Route::get('auth/login', 'Auth\AuthController@getLogin')->name('auth.login');
Route::post('auth/login', 'Auth\AuthController@postLogin')->name('auth.login');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name("auth.logout");


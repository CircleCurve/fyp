<?php

namespace App\Http\Controllers\Auth;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ActionLogRepository;


class PermissionController extends Controller
{
    protected $actionlog ;

    public function __construct(ActionLogRepository $actionlog)
    {
        $this->actionlog = $actionlog;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $perm = new Permission();
        $perms = $perm->with(['roles'])->get() ;

        return view('perm.view', [
            'perms' => $perms
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $perms = Permission::get();

        return view('perm.create', [
            "perms" => $perms
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator=  \Validator::make($request->all(),[
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "name" => 'required|min:3|max:20|unique:permissions',
            'display_name' => 'required|min:3|max:20|unique:permissions',
            'description' => 'max:255',
        ]);

        if ($validator->fails()){
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }
        $perm = new Permission();

        $perm = $perm->create($request->all());


        if (!isset($perm->id)){
            $request->session()->flash('alert-danger', 'Insert role with database error');
            return redirect()->route('perm.create');
        }
        $this->actionlog->add('addPerm', json_encode($request->all()));


        $request->session()->flash('alert-success', 'Permission has successfully added');
        return redirect()->route('perm.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $perm = new Permission();
        $perm = $perm->where('id', $id)->first();


        //dd($role);
        return view('perm.edit', [
            "perm" => $perm,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator=  \Validator::make($request->all(),[
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "name" => 'required|min:3|max:20',
            'display_name' => 'required|min:3|max:20',
            'description' => 'max:255',
        ]);

        if ($validator->fails()){
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }

        $perm = Permission::find($id);
        if ($perm == null){
            $request->session()->flash('alert-danger', 'Permission does not exists');
            return back()->withInput()->withErrors($validator);
        }
        try {
            $isUpdate = $perm->update($request->all());
        } catch ( \Illuminate\Database\QueryException $e) {
            if ($e->errorInfo[0] == "23000"){
                $request->session()->flash('alert-danger', 'The name has been taken by another permission. Please add update to another one');
                return back()->withInput()->withErrors($validator);
            }

        }

        if ($isUpdate != true){
            $request->session()->flash('alert-danger', 'Database error of updating permission infomration');
            return back()->withInput()->withErrors($validator);
        }

        $this->actionlog->add('updatePerm', json_encode($request->all()));


        $request->session()->flash('alert-success', 'Permission has successfully updated');
        return redirect()->route('perm.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

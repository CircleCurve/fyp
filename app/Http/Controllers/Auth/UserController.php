<?php

namespace App\Http\Controllers\Auth;

use App\Actionlog;
use App\Permission;
use App\Repositories\UserRepository;
use App\Repositories\ActionLogRepository;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;

class UserController extends Controller
{
    protected $actionlog ;

    protected $userRepository;

    public function __construct(UserRepository $userRepository, ActionLogRepository $actionlog)
    {
        //$this->middleware('recapcha', ['only' => ['store']]);
        $this->userRepository = $userRepository;
        $this->actionlog = $actionlog;

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        if (!empty ($request->all()) && $request->isMethod("post")){

            $users = $this->userRepository->search($request)->get();

            $request->flash();

        }else{
            //$users = User::orderBy('created_at','desc')->get();
            //\DB::enableQueryLog();
            $users = User::with(['roles', 'actionlog'=> function($query){
                $query->orderBy('created_at', 'desc'); 
            }])->get();
            //dd($users->toArray()); 
            //dd($logs) ;
            //$logs = Auth::user()->actionlog()->orderBy('actionlog.id','desc')->get();
        }
        $roles = Role::all();

        return view('auth.view', [
            'users' => $users,
            'roles' => $roles
        ]);
    }

    public function search (Request $request) {
        return $this->index($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::with('perms')->get();
        $permissions = Permission::get();
        $rolePermission = [];

        foreach ($roles as $role){
            $rolePermission[$role->id] = [] ;
            foreach ($role->perms as $perm){

                array_push($rolePermission["$role->id"], $perm->id);
            }
        }
        return view('auth.register', [
            "roles" => $roles,
            "perms" => $permissions,
            "user_role_permission" => json_encode($rolePermission)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $validator=  \Validator::make($request->all(),[
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "name" => 'required|min:3|max:20',
            "email" => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'role' => 'required|array'
        ]);

        if ($validator->fails()){
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }

        //dd($request->all());

        $request->request->add(["status" => "1", "employeeStatus"=>"1"]);
        $request->request->set("password", bcrypt($request->get('password')));

        $user = new User();
        $user = $user->create($request->all());

        $user->attachRole(["id" => $request->get('role')]);

        $this->actionlog->add('addUser', json_encode( $request->all()));

        $request->session()->flash('alert-success', 'User has been added');

        return redirect()->route('user.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


        $user = User::where('id' , $id)->limit(1);
        $user = $user->with('roles.perms')->get();

        $roles = Role::with('perms')->get();
        $permissions = Permission::get();
        $rolePermission = [];
        $roleWithPermissions = [];

        $i = 0 ;
        $userWithRoles = [];
        foreach ($roles as $role){
            $rolePermission[$role->id] = [] ;

            foreach ($role->perms as $perm){
                array_push($rolePermission["$role->id"], $perm->id);
            }
        }

        foreach ($user[0]->roles as $userRole){
                array_push($userWithRoles, $userRole->id);

            foreach ($userRole->perms as $userPerms){
                array_push($roleWithPermissions, $userPerms->id);
            }

        }


        /*

        $logs = User::with(['Actionlog'])
            ->where('id', $id)
            ->orderBy('id','desc')->get();

        */
        return view('auth.detail',[
            'user' => $user[0],
            "roles" => $roles,
            "perms" => $permissions,
            "user_role_permission" => json_encode($rolePermission),
            'userWithRoles' => $userWithRoles,
            'roleWithPermissions' => $roleWithPermissions

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator=  \Validator::make($request->all(),[
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "name" => 'required|min:3|max:20',
            "email" => 'required|email|max:255',
            'password' => 'confirmed|min:6',
            'role' => 'required|array'
        ]);

        if ($validator->fails()){
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }

        try {
            $user = User::findOrFail($id);
            if (!empty($request->get('password'))){
                $password = bcrypt($request->get('password'));
                $request->request->set("password", bcrypt($request->get('password')));
                $all = $request->all();
            }else{
                $all = $request->except(['password']);
            }
            $isUpdate = $user->update($all);

        } catch ( \Illuminate\Database\QueryException $e) {
            if ($e->errorInfo[0] == "23000"){
                $request->session()->flash('alert-danger', 'The email has been taken by another account. Please add update to another one');
                return back()->withInput()->withErrors($validator);
            }

        }

        if ($isUpdate != true){
            $request->session()->flash('alert-danger', 'Database error of updating role infomration');
            return back()->withInput()->withErrors($validator);
        }
        $user->detachRoles();
        $user->attachRole(["id" => $request->get('role')]);

        $this->actionlog->add('updateUser', json_encode(["id"=> $id, "request"=>$request->all()]));

        $request->session()->flash('alert-success', 'Role with permission has successfully updated');
        return redirect()->route('user.index');
    }


    public function updatePersonalInformation(Request $request, $id){
        $validator=  \Validator::make($request->all(),[
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "firstName" => 'required|min:3|max:20',
            "lastName" => 'required|min:3|max:20',
            "hkid" => [
                'regex:/^[A-Za-z]{1}[0-9]{7}$/',
                'required'
            ],
            "directline" => 'required|min:8|max:8',
            "address" => 'required|max:255',
            'jobTitle' => 'required|max:255',
        ],[
            'hkid.regex' => 'The hkid format is wrong. A1234567',
        ]);

        if ($validator->fails()){
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }

        try {
            $user = User::findOrFail($id);

            $isUpdate = $user->update($request->all());

        } catch ( \Illuminate\Database\QueryException $e) {
            if ($e->errorInfo[0] == "23000"){
                $request->session()->flash('alert-danger', 'The email has been taken by another account. Please add update to another one');
                return back()->withInput()->withErrors($validator);
            }

        }

        if ($isUpdate != true){
            $request->session()->flash('alert-danger', 'Database error of updating role infomration');
            return back()->withInput()->withErrors($validator);
        }

        $this->actionlog->add('updateUser', json_encode(["id"=> $id, "request"=>$request->all()]));

        $request->session()->flash('alert-success', 'Personal information has successfully updated');
        return redirect()->route('user.index');
    }

    public function postStatus(Request $request, $id) {
        $user = User::findOrFail($id);
        $isUpdated = $user->update($request->all());

        if ($isUpdated != true){
            $request->session()->flash('alert-danger', 'Database error of updating  infomration');
            return back();
        }

        $this->actionlog->add('updateStatus', json_encode(["id"=> $id, "request"=>$request->all()]));

        $request->session()->flash('alert-success', 'User status has successfully updated');
        return redirect()->route('user.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $user = User::findOrFail($id);
        $isDeleted = $user->delete(); // This will work no matter what

        if ($isDeleted != true){
            $request->session()->flash('alert-danger', 'Database error of delete  infomration');
            return back();
        }

        $request->session()->flash('alert-success', 'User has successfully deleted');
        return redirect()->route('user.index');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Repositories\ActionLogRepository;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins {
        AuthenticatesAndRegistersUsers::postLogin as traitPostLogin;
        AuthenticatesAndRegistersUsers::getLogout as traitGetLogout;
        AuthenticatesAndRegistersUsers::postRegister as traitPostRegister;
    }

    protected $actionlog ;
    //protected $redirectPath = '/dashboard';
    //protected $loginPath = '/login';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    protected $redirectPath = '/';
    public function __construct(ActionLogRepository $actionlog)
    {
        $this->middleware('recapcha', ['only' => ['postLogin']]);
        $this->middleware('guest', ['except' => ['getLogout']]);
        $this->actionlog = $actionlog;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),

        ]);
    }

    public function postLogin(Request $request){
        $this->validate($request, [
            $this->loginUsername() => 'required|email',
            'password' => 'required',
        ]);

        $postLogin =  $this->traitPostLogin($request);

        if (isset(Auth::user()->status) && Auth::user()->status != 1){
            Auth::logout();
            return back()
                ->withInput($request->only($this->loginUsername(), 'remember'))
                ->withErrors([
                    $this->loginUsername() => "Your account has been banned. Please contact an Administrator",
                ]);
        }


        if (Auth::check()){
            $this->actionlog->add('login');
        }
        return $postLogin;
    }

    public function postRegister(Request $request){
        /*
        $this->validate($request, [
            $this->loginUsername() => 'required|email',
            'password' => 'required',
        ]);

        $this->throwValidationException();

        */

        return $this->traitPostRegister($request);
    }

    public function getLogout(){
        $this->actionlog->add('logout');

        return $this->traitGetLogout();
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ActionLogRepository;


class RoleController extends Controller
{
    protected $actionlog ;

    public function __construct(ActionLogRepository $actionlog)
    {
        $this->actionlog = $actionlog;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = Role::get();

        return view('role.view', [
            'roles' => $roles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $perms = Permission::get();

        return view('role.create', [
            "perms" => $perms
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator=  \Validator::make($request->all(),[
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "name" => 'required|min:3|max:20|unique:roles',
            'display_name' => 'required|min:3|max:20|unique:roles',
            'description' => 'max:255',
            'perms' => 'required|array'
        ]);

        if ($validator->fails()){
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }
        $role = new Role();

        $role = $role->create($request->all());


        if (!isset($role->id)){
           $request->session()->flash('alert-danger', 'Insert role with database error');
            return redirect()->route('role.create');
        }

        $role->attachPermission(["id" => $request->get('perms')]);
        $this->actionlog->add('addRole', json_encode(["request" => $request->all(), "return" => $role->id]));


        $request->session()->flash('alert-success', 'Role with permission has successfully added');
        return redirect()->route('role.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $role = new Role();
        $role = $role->where('id', $id)->with('perms')->first();
        $perms = Permission::get();
        $rolePermission =  [] ;

        foreach($role->perms as $selectPerms){
            array_push($rolePermission, $selectPerms->id);
        }
        //dd($role);
        return view('role.edit', [
            'role' => $role,
            "perms" => $perms,
            'rolePermission' => json_encode($rolePermission)
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator=  \Validator::make($request->all(),[
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "name" => 'required|min:3|max:20',
            'display_name' => 'required|min:3|max:20',
            'description' => 'max:255',
            'perms' => 'required|array'
        ]);

        if ($validator->fails()){
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }

        $role = Role::find($id);
        if ($role == null){
            $request->session()->flash('alert-danger', 'Role does not exists');
            return back()->withInput()->withErrors($validator);
        }
        try {
            $isUpdate = $role->update($request->all());
        } catch ( \Illuminate\Database\QueryException $e) {
            if ($e->errorInfo[0] == "23000"){
                $request->session()->flash('alert-danger', 'The name has been taken by another role. Please add update to another one');
                return back()->withInput()->withErrors($validator);
            }

        }

        if ($isUpdate != true){
            $request->session()->flash('alert-danger', 'Database error of updating role infomration');
            return back()->withInput()->withErrors($validator);
        }
        $role->detachPermissions($role->perms);
        $role->attachPermission(["id" => $request->get('perms')]);

        $this->actionlog->add('updateRole', json_encode( $request->all()));

        $request->session()->flash('alert-success', 'Role with permission has successfully updated');
        return redirect()->route('role.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $role = Role::findOrFail($id); // Pull back a given role
        $isDeleted = $role->delete(); // This will work no matter what

        if ($isDeleted != true){
            $request->session()->flash('alert-danger', 'Database error of delete  infomration');
            return back();
        }

        $this->actionlog->add('removeRole', json_encode( $request->all()));

        $request->session()->flash('alert-success', 'Role has successfully deleted');
        return redirect()->route('role.index');
    }
}

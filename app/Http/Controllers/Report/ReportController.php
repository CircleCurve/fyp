<?php

namespace App\Http\Controllers\Report;

use App\Repositories\ActionLogRepository;
use App\Repositories\SymptomsRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    protected $symptomsRepository, $actionlog;



    public function __construct(SymptomsRepository $symptomsRepository, ActionLogRepository $actionlog)
    {
        $this->symptomsRepository = $symptomsRepository;
        $this->actionlog = $actionlog;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // 3. user personal information
        // 6. distribute method change little bit more
        // 4. incident additional information
        // 5. allow to delete user and incident
        // 1. report create date
        // 2. data validation

        $symptoms = $this->symptomsRepository->getSymptomsAmount();

        return view('report.template', [
            'symptoms' => $symptoms
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

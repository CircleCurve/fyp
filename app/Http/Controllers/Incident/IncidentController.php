<?php

namespace App\Http\Controllers\Incident;

use App\Calendar;
use App\Incident;
use App\Jobs\SendReminderEmail;
use App\Progress;
use App\Repositories\ActionLogRepository;
use App\Repositories\CalendarRepository;
use App\Repositories\IncidentRepository;
use App\Repositories\SymptomsIssueRepository;
use App\Repositories\SymptomsRepository;
use App\Symptoms;
use App\SymptomsIssue;
use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Mail;
use Auth;

class IncidentController extends Controller
{
    protected $symptomsRepository, $symptomsIssueRepository,$incidentRepository, $calendarRepository, $actionlog;

    public function __construct(SymptomsRepository $symptomsRepository, SymptomsIssueRepository $symptomsIssueRepository, IncidentRepository $incidentRepository, CalendarRepository $calendarRepository, ActionLogRepository $actionlog)
    {
        $this->symptomsRepository = $symptomsRepository;
        $this->symptomsIssueRepository = $symptomsIssueRepository;
        $this->incidentRepository = $incidentRepository;
        $this->calendarRepository = $calendarRepository;
        $this->actionlog = $actionlog;
    }

    public function sendEmailReminder(Request $request, $symptoms)
    {
        //$user = User::findOrFail(31);
        $user = Auth::user();

        $title = $request->get("title");
        $appId = $request->get("appId");
        $description = $request->get("description");
        $link = route('incident.assigned');
        $arr = $request->all();

        $symptom_body = "" ;
        foreach ($symptoms as $symptom) {
            $symptom_body .= '
                    <tr>
                        <td align = "center" >'. $symptom->id.'</td>
                        <td align = "center" >'.$symptom->symptoms.'</td>
                    </tr >';
        }

        //$this->dispatch(new SendReminderEmail($user, $arr));

        //dd($symptoms);

        Mail::queue( 'emails.incident',

            ['user' => $user, 'title' => $title, "appId" => $appId, "symptom_body" => $symptom_body, "description" => $description, "link" => $link],

            function ($m) use ($user, $title) {
                //
                $m->from('hello@app.com', $title);

                $m->to($user->email , $user->name )->subject($title);
            });

        $this->actionlog->add('sendEmail', json_encode(["email"=> $user->email, "request"=>$request->all()]));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $assignIncidents = $this->incidentRepository->assign(Auth::user()->id, "assign");
        $receiveIncidents = $this->incidentRepository->assign(Auth::user()->id, "isAssigned");

        $event = [];
        $i = 0 ;

        foreach ($assignIncidents as $user){
            foreach ($user->incident as $incident) {
                $to = $incident->calendar->user->name;
                $incident->calendar->title = "[Assign to $to ] ". $incident->calendar->title;
                $event[$i++] = $incident->calendar->toArray();
            }
        }

        foreach ($receiveIncidents as $user){
            foreach ($user->calendar as $incident) {
                $incident->title = "[Receive] ". $incident->title;
                $event[$i++] = $incident->toArray();
            }
        }

        return view('incident.view', [
            'event'=> $event,
            'receiveIncidents' => $receiveIncidents
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $issue = $this->symptomsRepository->getIssue();
        $symptoms = $this->symptomsRepository->getSymptoms();

        return view('incident.create', [
            "issue" => $issue,
            "symptoms" => $symptoms
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $messages = [
            'account.regex' => 'The :attribute field only allow alphabet and integer.',
            'orderNo.regex' => 'The :attribute field only allow alphabet and integer.',
        ];

        $validator = \Validator::make($request->all(), [
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "title" => 'required|min:3|max:50',
            'appId' => 'required|integer',
            'account' => array('required', 'regex:/^[a-zA-Z0-9]{3,50}$/'),
            'orderNo' => array('required', 'regex:/^[a-zA-Z0-9]{3,50}$/'),
            'symptoms' => 'required',
            "description" => 'max:255',
        ], $messages);


        if ($validator->fails()) {
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }


        //$request->request->add(["status" => "0"]);
        $request->request->set("user_id", Auth::user()->id);

        $request->request->set("expected_time", date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date("m"), date("d") + 3, date("Y"))));
        $request->request->set("ip", $request->ip());

        ////////add request
        $this->add($request);

        $request->session()->flash('alert-success', 'Incident has been added');

        $symptoms = Symptoms::whereIn('id', explode(',', $request->get('symptoms')))->get();
        $this->sendEmailReminder($request, $symptoms);

        $this->actionlog->add('addIncident', json_encode(["request"=>$request->all()]));

        return redirect()->route('incident.index');
        //d2($request->all());
    }

    public function add(Request $request){
        $saveIncident = Incident::create($request->all());
        $lastIncidentId = $saveIncident->id;
        $this->symptomsIssueRepository->add($lastIncidentId, $request->get('symptoms'));
        $lessIncidentFirsts = $this->incidentRepository->lessIncidentFirst();
        $this->calendarRepository->add($request,$lastIncidentId, $lessIncidentFirsts[0]->id);

        $saveIncident->progress()->attach(1, [
            'user_id' => Auth::user()->id,
            'title' => Auth::user()->name. ' has sended email to '. $lessIncidentFirsts[0]->name,
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $progresses = Progress::get();
        $incident = Incident::findOrFail($id);
        $issue = $this->symptomsRepository->getIssue();
        $symptoms = $this->symptomsRepository->getSymptoms();
        $affectSymptoms = SymptomsIssue::where('incident_id', $incident->id)->get();
        $affect = [] ;

        //===========Progress==============


        $timelines = Incident::where('id', $id)->with(['progress' => function($q){
            $q->orderBy('pivot_created_at', 'desc');
        }, 'progress.user'])->get();


        //dd($timelines->toArray());

        $timeline = [];

        foreach ($timelines as  $incident){
            foreach ($incident->progress as $progress){
                $progressCreatedAt = $progress->pivot->created_at;
                $createYMD = explode(" ",$progressCreatedAt)[0];
                $timeline[$createYMD] = isset($timeline[$createYMD]) ?  $timeline[$createYMD] : [];
                array_push( $timeline[$createYMD]  , $progress);
            }
        }

        //dd($timelines->toArray());
        $i = 0 ;
        foreach($affectSymptoms as $affectSymptom){
            $affect[$i++] = (string)$affectSymptom->symptoms_id;
        }
        //$affect = rtrim($affect, ",");

        if (!Auth::user()->id == $incident->user_id){
            dd("error");
        }


        return view('incident.details',[
            'incident' => $incident,
            'issue' => $issue,
            'symptoms'=>$symptoms,
            'affect' => json_encode($affect),
            'progresses' => $progresses,
            'timeline' => $timeline
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = \Validator::make($request->all(), [
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"
            'symptoms' => 'required',
            "description" => 'max:255',
        ]);


        if ($validator->fails()) {
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }

        SymptomsIssue::where('incident_id', $id)->delete();

        $this->symptomsIssueRepository->add($id,$request->get('symptoms'));

        Incident::where('id', $id)->update([
           'description' => $request->get('description')
        ]);

        $this->actionlog->add('updateIncident', json_encode($request->all()));

        $request->session()->flash('alert-success', 'Incident has been updated');


        return redirect()->route('incident.show', $id);


        //Incident::updated($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function assigned(){
        //$incidents = Auth::user()->calendar()->get();
        $user = Auth::user();

        $assignIncidents = $this->incidentRepository->assign($user->id, 'assign');
        $receiveIncidents = $this->incidentRepository->assign($user->id, 'isAssigned');

        return view('incident.assigned', [
            'assignIncidents'=>$assignIncidents,
            'receiveIncidents'=>$receiveIncidents,
            'searchIncidents'=>[],

        ]);


    }

    public function reassign(Request $request, $id){
        //$incidents = Auth::user()->calendar()->get();

        $lessIncidentFirsts = $this->incidentRepository->lessIncidentFirst();

        $this->calendarRepository->reassign($id, $lessIncidentFirsts[0]->id);

        $incident = Incident::find($id);
        $incident->progress()->attach(1, [
            'user_id' => $lessIncidentFirsts[0]->id,
            'title' => Auth::user()->name. ' has reassigned incident to '. $lessIncidentFirsts[0]->name,
            'description' => ''
        ]);

        $this->actionlog->add('reassign', json_encode([
            'title' => Auth::user()->name. ' has reassigned incident to '. $lessIncidentFirsts[0]->name,
            'description' => ''
        ]));

        $request->session()->flash('alert-success', 'Incident has been reassign');

        return back();


    }

    public function search(Request $request){
        $user = Auth::user();

        $searchIncidents = $this->incidentRepository->search($request)->get();
        $assignIncidents = $this->incidentRepository->assign($user->id, 'assign');
        $receiveIncidents = $this->incidentRepository->assign($user->id, 'isAssigned');


        return view('incident.assigned', [

            'assignIncidents'=>$assignIncidents,
            'receiveIncidents'=>$receiveIncidents,
            'searchIncidents'=>$searchIncidents,

        ]);

        //dd($search->get()->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateProgress(Request $request, $id)
    {
        //
        $validator = \Validator::make($request->all(), [
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"
            'progress' => 'required|integer',
            "progress_description" => 'max:255',
        ]);


        if ($validator->fails()) {
            $request->session()->flash('alert-danger', 'There is some error. Please check it ');
            return back()->withInput()->withErrors($validator);
        }



        $incident = Incident::find($id);
        $calendar = Calendar::find($id);
        $progress = Progress::where("id", $request->get('progress'))->first();
        //$progress = Progress::find(1);

        $calendar->update([
            "color" =>$progress->color
        ]);
        $incident->progress()->attach($request->get('progress'), [
            'user_id' => Auth::user()->id,
            'title' => Auth::user()->name. ' has changed progress to '. $progress->name,
            'description' => $request->get('progress_description')
        ]);

        /*
        SymptomsIssue::where('incident_id', $id)->delete();

        $this->symptomsIssueRepository->add($id,$request->get('symptoms'));

        Incident::where('id', $id)->update([
            'description' => $request->get('description')
        ]);*/
        $this->actionlog->add('updateProgress', json_encode([
            'id' =>     $id,
            'request'=> $request->all()
        ]));

        $request->session()->flash('alert-success', 'Incident has been updated');


        return redirect()->route('incident.show', $id);


        //Incident::updated($request->all());
    }
}

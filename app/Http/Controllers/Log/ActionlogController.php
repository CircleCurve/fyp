<?php

namespace App\Http\Controllers\Log;

use App\Actionlog;
use App\Repositories\ActionLogRepository;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use View;


class ActionlogController extends Controller
{
    protected $actionlog ;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ActionLogRepository $actionlog)
    {
        $this->actionlog = $actionlog; 
    }



    public function index(Request $request)
    {
        if (!empty ($request->all())){

            $logs = $this->actionlog->search($request)->get();
            $request->flash();

        }else{
            //$logs = Actionlog::with(['User'])->orderBy('actionlog.id','desc')->get();
            $actionlog = new Actionlog();
            $logs = $actionlog->orderBy('id','desc')->with(['user'])->get();

            //dd($logs->toArray());
            //$logs = Auth::user()->actionlog()->orderBy('actionlog.id','desc')->get();
        }

        return view('log.log', [
            'logs' => $logs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

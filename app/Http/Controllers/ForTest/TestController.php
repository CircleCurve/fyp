<?php

namespace App\Http\Controllers\ForTest;

use App\actionlog;
use App\Member_acc;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    


    public function __construct()
    {
        $this->middleware('recapcha', ['only'=>['store']]) ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        
        return redirect('/create');
        //return 'index' ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        return view('tasks.fortest', [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //s
        //

        $validator=  \Validator::make($request->all(),[
            //"uid" =>"required|integer|max:255|unique:member_acc,uid"

            "name" => [
                'regex:/^[A-Za-z0-9]{4,16}$/',
                "required",
                "unique:member_acc,name"
            ],
            "password" => "required|digits_between:5,16|confirmed",
        ],[
            //"required"=>"please input something",


            "name.regex" => "Please input 4-16 account(2)",
            "name.required" => "Please input 4-16 account",
            "name.unique"=> "Please input the other name",
            //"name.string" => "Please input string account" ,
            //"name.min" => "please input 4 digit over account ",
            "password.required"=>"Please input 5-16 password",
        ]);

        if ($validator->fails()){
            //return redirect('/create')->withErrors(['msg', "Error Code from some where" ]);
            return redirect("/create")
                ->withInput($request->except("password"))
                ->withErrors($validator->errors()->all());
        }



        try {
            $member = Member_acc::create([
                "source" => "99",
                "name" => $request->name,
                "password" => md5($request->password),
                "token" => md5($request->password),
                "ip" => $request->getClientIp()

            ]);

            actionlog::create([
                "uid" => (int)$member->id,
                "type"=>"reg",
                "remark" => "成功註冊",
                "ip" => $request->getClientIp(),

            ]);
        }catch (\Exception $e){

            return redirect('/create')->withErrors([ "資料庫載入時發生錯誤 Error Code : ".$e->getCode() ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

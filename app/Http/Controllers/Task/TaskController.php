<?php

namespace App\Http\Controllers\Task;

use Illuminate\Support\Facades\Validator;
use App\Repositories\TaskRepository;
use App\Task;
use App\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    protected  $tasks;
    //
    public function __construct(TaskRepository $tasks)
    {
        //$this->middleware('auth') ;

        $this->tasks = $tasks;
    }

    public function store(Request $request){

        $this->validate($request,[
           "name"=>"required|max:255|unique:tasks,name"
        ]);

        /*
        $request->user()->task()->create([
            'name' => $request->name,
        ]);
        */

        return redirect('/tasks');
    }

    public function index(Request $request){
        //task = Task::where('user_id',$request->user()->id)->get();

        return view('tasks.task',[
            'tasks'=>$this->tasks->getTask($request->user())
        ]);
        /*
        return view('tasks.task',[
            'tasks'=>$this->tasks->forUser($request->user())

        ]);*/
    }

    public function destroy(Request $request,Task $task){

        $this->authorize('destroy',$task) ;

        $task->delete();

        return redirect('/tasks');
    }

}

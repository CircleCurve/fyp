<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SymptomsIssue extends Model
{
    //
    protected $table = 'symptomsIssue';

    protected $fillable = ['incident_id', 'symptoms_id', 'status'];


    public function symptoms(){
        return $this->belongsTo('App\Symptoms');
    }

    public function incident(){
        return $this->belongsTo('App\Incident');
    }
}

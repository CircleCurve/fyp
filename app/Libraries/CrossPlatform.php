<?php
namespace App\Libraries ;

class CrossPlatform {

    public function post_url_contents($url, $fields) {
        $fields_string = "";

        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . urlencode($value) . '&';
        }
        $fields_string = rtrim($fields_string, '&');
        $crl = curl_init();
        $timeout = 15;
        curl_setopt($crl, CURLOPT_URL, $url);
        curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($crl, CURLOPT_POST, count($fields));
        curl_setopt($crl, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
        $ret = curl_exec($crl);
        if ($ret === false) {
            echo 'Curl error: ' . curl_error($crl);
        }
        curl_close($crl);
        return $ret;
    }

    public function post_url_contents_headers($url, $fields, $headers = array()) {
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // 送出 post, 並接收回應, 存入 $result
        $result = curl_exec($ch);
        echo $result;
    }

    public function get_url_contents($url) {
        $crl = curl_init();
        $timeout = 15;
        curl_setopt($crl, CURLOPT_URL, $url);
        curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
        $ret = curl_exec($crl);
        if ($ret === false) {
            echo 'Curl error: ' . curl_error($crl);
        }
        curl_close($crl);
        return $ret;
    }

}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actionlog extends Model
{
    //
    protected $table = 'actionlog';

    protected $fillable = ['user_id', 'type', 'description', 'ip', 'extra'];

    public function user(){
        return $this->belongsTo('App\User'); 
    }

    public function incident() {
        return $this->belongsToMany('App\Incident')->withTimestamps();
    }
}

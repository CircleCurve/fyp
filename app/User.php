<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract,
                                    //AuthorizableContract,
                                    CanResetPasswordContract
{
    //use Authenticatable, Authorizable, CanResetPassword, EntrustUserTrait; // add this trait to your user model
    use Authenticatable, CanResetPassword;
    use EntrustUserTrait;
    use SoftDeletes { SoftDeletes::restore insteadof EntrustUserTrait; }    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'source',
        'name',
        'email',
        'password' ,
        'status',
        'ip',
        'description',
        'remark',
        'employeeStatus',
        "firstName",
        "lastName",
        "sex",
        "hkid",
        "birthday",
        "directline",
        "address",
        'jobTitle'
    ];

    protected $dates = ['deleted_at'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function actionlog() {
        return $this->hasMany('App\Actionlog');
    }


    public function incident(){
        return $this->hasMany('App\Incident');

    }

    public function calendar(){
        return $this->hasMany('App\Calendar');
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    //
    //
    protected $table = "incident";

    protected $fillable = ['title', 'appId', 'account', 'orderNo','status','user_id','description', 'completed_time','expected_time', 'ip'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function calendar()
    {
        return $this->hasOne('App\Calendar');
    }

    public function progress()
    {
        return $this->belongsToMany('App\Progress')->withTimestamps()->withPivot('title', 'description');
    }

    public function actionlog() {
        return $this->belongsToMany('App\Actionlog')->withTimestamps();
    }
}

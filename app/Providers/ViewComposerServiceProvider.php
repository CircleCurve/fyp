<?php

namespace App\Providers;

use App\Repositories\IncidentRepository;
use Illuminate\Support\ServiceProvider;
use Auth;
use Route;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //


        view()->composer("admin.*", function($view){
            if (Auth::user() != null){
                $incidentRepo = new IncidentRepository();
                $incidents = $incidentRepo->assign(Auth::user()->id, "stillValid");
                $incidentsAssign = $incidentRepo->assign(Auth::user()->id, "stillValidAssign");

                $view->with('stillValidIncidents', $incidents);
                $view->with('stillValidIncidentsAssign', $incidentsAssign);
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

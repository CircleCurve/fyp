<?php

namespace App\Jobs;

use App\User;
use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Mail;

class SendReminderEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user, $arr;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $arr)
    {
        //
        $this->user = $user;
        $this->arr = $arr;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        //

        $arr = $this->arr;
        $user = $this->user;
        $title = $arr["title"];
        $appId = $arr["appId"];
        $symptoms = $arr["symptoms"];
        $description = $arr["description"];
        $link = route('incident.create');

/*
        Mail::send('emails.incident',

            ['user' => $user, 'title' => $title, "appId" => $appId, "symptoms" => $symptoms, "description" => $description, "link" => $link],

            function ($m) use ($user, $title) {
                //
                $m->from('hello@app.com', $title);

                $m->to($user->email, $user->name)->subject($title);
            });

*/        /*
        $mailer->send('emails.reminder', ['user' => $this->user], function ($m) {
            //
        });*/
    }
}

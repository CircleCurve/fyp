<?php
namespace App;
class Validator {
	private $format ;
	private $msg ;
	private $param = array() ;
	private $error = array() ;
	private $beforeVerify ;
	private $afterVerify ;

	public  function makeValidator($param , $formatMsg){
		$this->param = $param ;
		$this->format = $formatMsg[0] ;
		$this->msg = $formatMsg[1] ;
	}


	public function verify($param , $format, $msg){

		foreach ($format as $key => $regexType){

			if (!is_array($regexType)){
				$regex = explode( "|" , $regexType) ;

			}else{
				$regex = $regexType ; 
			}
			$status = 0 ;

			if (in_array("required", $regex)) {

				if (isset($param["$key"]) && !empty($param["$key"])){
					$status = 1;
				}else{
					$status = -1 ;
				}

			}elseif (in_array("optional", $regex)){
				if (isset($param["$key"]) && !empty($param["$key"])){
					$status = 1;
				}

			}

			if ($status === 1) {
				$this->checkFormat($regex, $param[$key],$key,  $msg) ;
			}elseif($status === -1){

				$msgKey =  $key.".required" ;
				$this->pushError(array($msgKey => $msg["$msgKey"])) ;

			}else{

			}

			//return fail() ;
		}


	}

	public function checkFormat($regex , $param,$key , $msg) {
		//echo $key ;
		//echo $param ;
		$dataFormat = new DataFormat() ;
		$regexLength = count($regex) ;
		$error = array() ;
		$flexibleFuncParam = array() ;

		for ($i = 0 ; $i<$regexLength; $i++){
			//echo $regex[$i] ."<br />" ;
			$paramKeyValue = ucfirst(strtolower($regex[$i])) ;
			$paramKey = explode(":",$paramKeyValue) ;
			$funcName = "is".$paramKey[0]."Valid" ;

			if (method_exists($dataFormat,$funcName) === true){

				//$isValid = $dataFormat->{$funcName}($param) ;
				array_push($flexibleFuncParam, $param) ;

				if (isset($paramKey[1])){
					$paramKey[1] = explode(",", $paramKey[1]) ;

					$flexibleFuncParam = array_merge($flexibleFuncParam, $paramKey[1]);
				}

				$isValid = call_user_func_array(array($dataFormat,$funcName),$flexibleFuncParam);

				if ($isValid === false){

					//$msgKey =  $key.".".$regex[$i] ;
					$msgKey =  $key.".". strtolower ($paramKey[0]) ;
					//$msgKey = $paramKey[0] ;
						
					if (isset( $msg["$msgKey"])){

						$error = array($msgKey => $msg["$msgKey"]);
					}else{
						$error = array($regex[$i] => "default message");

					}

				}

			}

		}

		$this->pushError($error) ;

	}

	public function fail() {

		$this->callback($this->beforeVerify) ;
		if ($this->haveError() !== true){
			$this->verify($this->param , $this->format, $this->msg) ;
		}

		if ($this->haveError() !== true){
			$this->callback($this->afterVerify);
		}

		return $this->haveError() ;
	}

	public function haveError(){
		if (!empty($this->error)){
			return true ;
		}

		return false ;
	}


	public function withError() {
		return $this->error ;
	}

	private function pushError($error) {
		$this->error = array_merge($this->error, $error) ;
	}

	private function is_closure($t) {
		return is_object($t) && ($t instanceof \Closure);
	}

	public function addError($field, $msg){
		$error = array("$field"=>"$msg") ;
		$this->pushError($error) ;
	}

	public function before($callback){
		$this->beforeVerify = $callback ;
	}

	public function after($callback){
		$this->afterVerify = $callback ;
	}

	public function callback( $closure){
		if ($this->is_closure($closure)){
			$closure($this);
		}
	}

	public function push($key, $val){
		$this->param["$key"] =  $val ;
	}

	public function get($key){

		if (!isset($this->param["$key"])){
			return "" ; 
		}
		return $this->param["$key"] ;
	}
}

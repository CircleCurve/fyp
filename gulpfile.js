var gulp = require('gulp')
, modifyCssUrls = require('gulp-modify-css-urls');
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
/*
elixir(function(mix) {
    mix.sass('app.scss');
});
*/
/*
<script src="{{ asset("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js")}}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js")}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset("/bower_components/AdminLTE/dist/js/app.min.js")}}"></script>
*/


gulp.task("copyfiles", function () {
   gulp.src('vendor/bower_dl/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')
       .pipe(gulp.dest("resources/assets/js/"));
    gulp.src('vendor/bower_dl/AdminLTE/bootstrap/js/bootstrap.min.js')
        .pipe(gulp.dest("resources/assets/js/"));
    gulp.src('vendor/bower_dl/AdminLTE/dist/js/app.min.js')
        .pipe(gulp.dest("resources/assets/js/"));
    //===============For Plugin============================
    gulp.src('vendor/bower_dl/AdminLTE/plugins/iCheck/icheck.min.js')
        .pipe(gulp.dest("resources/assets/js/plugin"));

    gulp.src('vendor/bower_dl/bootstrap-validator/dist/validator.min.js')
        .pipe(gulp.dest("resources/assets/js/plugin"));

    gulp.src('vendor/bower_dl/AdminLTE/plugins/datatables/jquery.dataTables.min.js')
        .pipe(gulp.dest("resources/assets/js/plugin"));

    gulp.src('vendor/bower_dl/AdminLTE/plugins/daterangepicker/daterangepicker.js')
        .pipe(gulp.dest("resources/assets/js/plugin"));

    gulp.src('vendor/bower_dl/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js')
        .pipe(gulp.dest("resources/assets/js/plugin"));
    //===============For Plugin============================

    //=================for css==============================
    gulp.src('vendor/bower_dl/AdminLTE/bootstrap/css/bootstrap.min.css')
        .pipe(gulp.dest("resources/assets/css/"));
    gulp.src('vendor/bower_dl/AdminLTE/dist/css/AdminLTE.min.css')
        .pipe(gulp.dest("resources/assets/css/"));
    gulp.src('vendor/bower_dl/AdminLTE/dist/css/skins/skin-blue.min.css')
        .pipe(gulp.dest("resources/assets/css/"));


    gulp.src('vendor/bower_dl/AdminLTE/plugins/iCheck/square/blue.css')
        .pipe(modifyCssUrls({
            modify: function (url, filePath) {
                return url;
            },
            prepend: '/assets/img/icheck/',
            append: ''
        }))
        .pipe(gulp.dest("resources/assets/css/plugin"));

    gulp.src('vendor/bower_dl/AdminLTE/dist/css/skins/_all-skins.min.css')
        .pipe(gulp.dest("resources/assets/css"));

    gulp.src('vendor/bower_dl/AdminLTE/plugins/datatables/dataTables.bootstrap.css')
        .pipe(gulp.dest("resources/assets/css"));

    gulp.src('vendor/bower_dl/AdminLTE/plugins/daterangepicker/daterangepicker.css')
        .pipe(gulp.dest("resources/assets/css"));

    //=================for css==============================


});

elixir(function(mix) {
    mix.scripts(
        [
            'js/jquery-2.2.3.min.js',
            'js/bootstrap.min.js',
            'js/app.min.js',
            'js/plugin/icheck.min.js',
            'js/plugin/validator.min.js',
            'js/plugin/jquery.dataTables.min.js',
            'js/plugin/dataTables.bootstrap.min.js',
            'js/plugin/daterangepicker.js',
        ],
        'public/assets/js/admin.js',
        'resources/assets'
    );

    mix.styles(
        ['AdminLTE.min.css','skin-blue.min.css', 'daterangepicker.css'],
        'public/assets/css/all.css'
    );

    mix.styles(
        ['AdminLTE.min.css', 'plugin/blue.css', '_all-skins.min.css'],
        'public/assets/css/login.css'
    );

    mix.styles(
        ['dataTables.bootstrap.css'],
        'public/assets/css/view_role.css'
    );

});
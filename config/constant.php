<?php
/**
 * Created by PhpStorm.
 * User: patrick
 * Date: 17年1月16日
 * Time: 上午1:29
 */

return [
    

    'actionlog' => [
        'login' => ['id'=>'1', 'msg' => 'Login Success'],
        'logout' => ['id'=>'2', 'msg' => 'Logout'],
        'addUser' => ['id'=>'3', 'msg' => 'add user : %s'],
        'removeUser' => ['id'=>'4', 'msg' => 'remove user : %s'],
        'addIncident' => ['id'=>'5', 'msg' => 'add incident : %s'],
        'updateIncident' => ['id'=>'6', 'msg' => 'update incident : %s'],
        'sendEmail' => ['id'=>'7', 'msg' => 'send email to user : %s'],
        'removeSymptoms' => ['id'=>'8', 'msg' => 'remove symptoms : %s'],
        'assign' => ['id'=>'9', 'msg' => 'assign incident to user : %s'],
        'addRole' => ['id'=>'10', 'msg' => 'add role : %s'],
        'removeRole' => ['id'=>'11', 'msg' => 'remove role : %s'],
        'updateRole' => ['id'=>'12', 'msg' => 'update role : %s'],
        'addPerm' => ['id'=>'13', 'msg' => 'add permission : %s'],
        'updatePerm' => ['id'=>'14', 'msg' => 'update permission :%s'],
        'report' => ['id'=>'15', 'msg' => 'view report'],
        'updateUser' => ['id'=>'16', 'msg' => 'update user : %s'],

        'updateProgress' => ['id'=>'17', 'msg' => 'update progress : %s'],
        'updateStatus' => ['id'=>'18', 'msg' => 'update status : %s'],
        'reassign' => ['id'=>'19', 'msg' => 'reassign incident to user : %s'],


        // etc
    ],
    'accStatus' => [
        '1' => ['color' => 'green' , 'msg' =>'Normal'],
        '-1' => ['color' => 'red' , 'msg' =>'Banned'],
    ],
    'employeeStatus' =>[
        '1' => ['color' => 'green' , 'msg' =>'Normal'],
        '2' => ['color' => 'green' , 'msg' =>'Sick leave'],
        '3' => ['color' => 'green' , 'msg' =>'Day off'],
    ],
    'calendar'=>[
        'setting'=>[
           1=> ["status"=>1, "color" => "#378006", "name" => "Assigned", "percentage" => "25%"],
           2=> ["status"=>2, "color" => "#78B4B4", "name" => "completed", "percentage" => "100%"],
           3=> ["status"=>3, "color" => "#57251D", "name" => "In Review", "percentage" => "50%"],
           4=> ["status"=>4, "color" => "#32D20B", "name" => "Communicating with front end staff", "percentage" => "50%"],
        ]
    ]
];
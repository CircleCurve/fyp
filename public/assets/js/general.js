/* Our main filter function
 * We pass the column location, the start date, and the end date
 */

var filterByDate = function (column, startDate, endDate) {
    // Custom filter syntax requires pushing the new filter to the global filter array

    $.fn.dataTableExt.afnFiltering.push(
        function (oSettings, aData, iDataIndex) {
            var rowDate = normalizeRowDate(aData[column]),
                start = normalizeDate(startDate),
                end = normalizeDate(endDate);

            // If our date from the row is between the start and end
            if (rowDate === "NaNaNaN") {
                console.log("NAN");
                return false;
            } else if (start <= rowDate && rowDate <= end) {
                return true;
            } else if (rowDate >= start && end === '' && start !== '') {
                return true;
            } else if (rowDate <= end && start === '' && end !== '') {
                return true;
            } else {
                return false;
            }
        }
    );
};

// converts date strings to a Date object, then normalized into a YYYYMMMDD format (ex: 20131220). Makes comparing dates easier. ex: 20131220 > 20121220
var normalizeDate = function (dateString) {
    var date = new Date(dateString);
    var normalized = date.getFullYear() + '' + (("0" + (date.getMonth() + 1)).slice(-2)) + '' + ("0" + date.getDate()).slice(-2);
    //var normalized = date.format('YYYY') + date.format('M') +date.format('D');
    return normalized;
}


// converts date strings to a Date object, then normalized into a YYYYMMMDD format (ex: 20131220). Makes comparing dates easier. ex: 20131220 > 20121220
var normalizeRowDate = function (dateString) {
    if (dateString == "") {
        return '';
    }
    var date = new Date(dateString.replace(/-/g, "/"));
    var normalized = date.getFullYear() + '' + (("0" + (date.getMonth() + 1)).slice(-2)) + '' + ("0" + date.getDate()).slice(-2);
    //var normalized = date.format('YYYY') + date.format('M') +date.format('D');
    console.log(normalized);
    return normalized;
}

var filterDataTable = function (filterDataTable, filtercol, filterDateId) {

    var tableSel = $('#' + filterDataTable);

    var tableSel2 = tableSel.DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "aaSorting": []
    });

    if (filterDateId === undefined){
        filterDateId = "reservation";
    }

    $('#' + filterDateId).daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD',
            },
        },
        function (start, end, label) {
            filterByDate(filtercol, start, end); // We call our filter function
            tableSel.dataTable().fnDraw();

        });

    $('#' + filterDateId).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
    });

    $('#' + filterDateId).on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    $('#clearFilter').on('click', function (e) {
        e.preventDefault();
        $(".searchForm").val("");
        tableSel2.search('')
            .columns().search('')
            .draw();
        $.fn.dataTableExt.afnFiltering.length = 0;
        tableSel.dataTable().fnDraw();
    });

    $('#'+filterDataTable +' tfoot th[class="need"]').each(function () {
        var title = $(this).text();
        $(this).html('<input style="width:100%" type="text" class="form-control searchForm" placeholder="Search ' + title + '" />');
        $('#'+filterDataTable +' tfoot tr').appendTo('#'+filterDataTable +' thead');
    });

    // Apply the search
    tableSel2.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value)
                    .draw();
            }
        });

    });
};

var highChartTable = function (id,tableId, name, type){
    Highcharts.chart(id, {
        data: {
            table: tableId
        },
        chart: {
            type: type
        },
        title: {
            text: name
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Units'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });
}
